package com.dolphin.mobility.base.network

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
object NetworkApiConstants {
    // Request OTP for user
    const val REQUEST_OTP = "server/otp/mobile/sendotp"

    // Verify OTP for User
    const val VERIFY_OTP = "server/otp/mobile/verify"

    //Fetch the ads playlist from server
    const val FETCH_ADS = "server/mobile/getplaylist"

    //Register device
    const val REGISTER_DEVICE = "server/setting/register-device"

    //Post device
    const val POST_DEVICE = "server/mobile/postdevice"

    // Search Google Places
    const val SEARCH_PLACES = "https://maps.googleapis.com/maps/api/place/autocomplete/json"

    // Get Details of selected place from the dropdown by it's id
    const val PLACE_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json"

    // Get the route / direction between two locations
    const val DIRECTION = "https://maps.googleapis.com/maps/api/directions/json"

    // Get Support Details
    const val GET_SUPPORT = "server/mobile/getsupport"

    // Get FAQ / Help Data
    const val GET_HELP = "server/mobile/getfaq"

    // Get Available Coupons
    const val GET_AVAILABLE_COUPONS = "server/mobile/getcouponlist"

    // Get Coupon
    const val GET_COUPON = "server/mobile/postusercoupon?"
}