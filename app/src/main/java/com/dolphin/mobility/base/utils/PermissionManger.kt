package com.dolphin.mobility.base.utils

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import com.dolphin.mobility.DolphinMobilityApp

/**
 * Created by Dhaval Parmar on 2019-05-21.
 * Email : dhvlparmar7@gmail.com
 */
object PermissionManager {

    fun askForPermission(): Boolean {
        return Build.VERSION.SDK_INT >= 23
    }

    fun hasStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            DolphinMobilityApp.instance,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
            DolphinMobilityApp.instance,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun hasCameraPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            DolphinMobilityApp.instance,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun hasLocationPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            DolphinMobilityApp.instance,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
            DolphinMobilityApp.instance,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }
}