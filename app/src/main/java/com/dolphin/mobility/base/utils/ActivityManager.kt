package com.dolphin.mobility.base.utils

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
object ActivityManager {

    /**
     * To start normal activity
     */
    fun startActivity(a: Activity, c: Class<*>) {
        a.startActivity(Intent(a, c))
    }

    /**
     * To start activity for result
     */
    fun startActivityForResult(a: Activity, c: Class<*>, requestCode: Int) {
        a.startActivityForResult(Intent(a, c), requestCode)
    }

    /**
     * To start activity with shared elements transitions
     */
    fun startActivityWithSharedTransition(activity: Activity, aClass: Class<*>, view: View) {
        val intent = Intent(activity, aClass)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity,
            view, ViewCompat.getTransitionName(view)!!
        )
        activity.startActivity(intent, options.toBundle())
        activity.finish()
    }

    /**
     * Finish existing work and start new activity
     */
    fun startFreshActivity(a: Activity, c: Class<*>) {
        a.apply {
            startActivity(Intent(a, c))
            finish()
        }

    }

    /**
     * To start new activity with passing data
     */
    fun startActivityWithBundle(activity: Activity, aClass: Class<*>, bundle: Bundle) {
        val intent = Intent(activity, aClass).putExtras(bundle)
        activity.startActivity(intent)
    }

    /**
     * To start activity for result with passing data
     */
    fun startActivityForResultWithBundle(activity: Activity, aClass: Class<*>, requestCode: Int, bundle: Bundle) {
        val intent = Intent(activity, aClass)
        intent.putExtras(bundle)
        activity.startActivityForResult(intent, requestCode)
    }

    /**
     * To start new activity and clear background activity stack
     */
    fun startFreshActivityClearStack(activity: Activity, aClass: Class<*>) {
        val intent = Intent(
            activity,
            aClass
        ).apply { Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK }
        activity.startActivity(intent)
        activity.finish()
    }

    /**
     * Actiivty navigation with animation
     * @param context    context
     * @param clazz      destination class name
     * @param enterAnim  Enter Animation resource id
     * @param exitAnim   Exit Animation resource id
     */
    fun navigateToActivityWithAnimation(context: Activity, clazz: Class<*>, enterAnim: Int, exitAnim: Int) {
        context.startActivity(Intent(context, clazz))
        context.overridePendingTransition(enterAnim, exitAnim)

    }

    /**
     * Actiivty navigation with animation
     * @param context    context
     * @param clazz      destination class name
     * @param enterAnim  Enter Animation resource id
     * @param exitAnim   Exit Animation resource id
     */
    fun navigateToFreshActivityWithAnimation(context: Activity, clazz: Class<*>, enterAnim: Int, exitAnim: Int) {
        context.startActivity(
            Intent(
                context,
                clazz
            ).apply { Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK })
        context.overridePendingTransition(enterAnim, exitAnim)
        context.finish()
    }


    /**
     * Actiivty navigation with animation
     * @param context    context
     * @param clazz      destination class name
     * @param enterAnim  Enter Animation resource id
     * @param exitAnim   Exit Animation resource id
     */
    fun navigateToActivityWithAnimationAndBundle(
        context: Activity,
        clazz: Class<*>,
        enterAnim: Int,
        exitAnim: Int,
        bundle: Bundle
    ) {
        val intent = Intent(context, clazz)
        intent.putExtras(bundle)
        context.startActivity(intent)
        context.overridePendingTransition(enterAnim, exitAnim)
    }
}