package com.dolphin.mobility.base.viewmodel

import android.app.Application
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dolphin.mobility.DolphinMobilityApp
import com.dolphin.mobility.base.network.NetworkApiClient
import com.dolphin.mobility.base.network.NetworkApiServiceCall
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import io.reactivex.disposables.Disposable

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
open class BaseViewModel(application: Application) : AndroidViewModel(application), Observable {

    @Transient
    private var mCallbacks: PropertyChangeRegistry? = null

    protected val mNetworkClient = NetworkApiServiceCall(NetworkApiClient.getNetworkServices())

    protected val errorMessageData = MutableLiveData<String>()

    protected val mInstance = DolphinMobilityApp.instance

    public lateinit var mDisposable: Disposable

    protected val pageNavigation = MutableLiveData<Int>()

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            if (mCallbacks == null) {
                return
            }
        }
        mCallbacks!!.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            if (mCallbacks == null) {
                mCallbacks = PropertyChangeRegistry()
            }
        }
        mCallbacks!!.add(callback)
    }

    fun notifyPropertyChanged(fieldId: Int) {
        synchronized(this) {
            if (mCallbacks == null) {
                return
            }
        }
        mCallbacks!!.notifyCallbacks(this, fieldId, null)
    }

    fun getErrorMessage(): LiveData<String> {
        return errorMessageData
    }

    fun setErrorMessage(errorMessage: String) {
        errorMessageData.postValue(errorMessage)
    }

    /**
     * To handle error got from api
     * @param message   String of error message
     */
    open fun handleApiErrors(message: String) {
        setProgress(FLAG_FALSE)
        errorMessageData.postValue(message)
    }

    /**
     * For callback to navigate sign up page
     */
    fun getPageRedirection(): LiveData<Int> {
        return pageNavigation
    }

    private val showProcess = MutableLiveData<Boolean>()

    fun getProgress(): LiveData<Boolean> {
        return showProcess
    }


    fun setProgress(boolean: Boolean) {
        showProcess.value = (boolean)
    }

    override fun onCleared() {
        if (::mDisposable.isInitialized)
            mDisposable.dispose()
        super.onCleared()
    }

}