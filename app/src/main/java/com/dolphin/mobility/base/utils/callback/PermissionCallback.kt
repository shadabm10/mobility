package com.dolphin.mobility.base.utils.callback

/**
 * Created by Dhaval Parmar on 2019-05-21.
 * Email : dhvlparmar7@gmail.com
 */
interface PermissionCallback {
    fun onPermissionResult(requestCode: Int, granted: Boolean)
}