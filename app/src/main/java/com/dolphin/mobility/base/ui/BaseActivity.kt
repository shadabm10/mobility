package com.dolphin.mobility.base.ui

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.transition.TransitionInflater
import android.transition.TransitionSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.dolphin.mobility.DolphinMobilityApp
import com.dolphin.mobility.R
import com.dolphin.mobility.base.utils.Constants.ADD_FRAGMENT
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.base.utils.Constants.FLAG_TRUE
import com.dolphin.mobility.base.utils.Constants.LOCATION_ENABLE_CALLBACK
import com.dolphin.mobility.base.utils.Constants.REMOVE_FRAGMENT
import com.dolphin.mobility.base.utils.Constants.REPLACE_FRAGMENT
import com.dolphin.mobility.base.utils.PermissionManager
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.base.utils.callback.PermissionCallback
import com.dolphin.mobility.base.viewmodel.BaseViewModel
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import java.util.*
import java.util.logging.Logger

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity(), LocationListener {
    protected lateinit var mViewDataBinding: T
    protected lateinit var mViewModel: V
    val mContext: Context get() = this
    private var mProgressDialog: DolphinProgressDialog? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var listener: PermissionCallback? = null
    private var mLocationRequest: LocationRequest? = null

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Override for set view model
     * @return view model instance
     */
    abstract fun getViewModel(): V

    /**
     * Override for set binding variable
     * @return variable id
     */
    abstract fun getBindingVariable(): Int

    /**
     * To return binding view
     */
    fun getViewDataBinding(): T {
        return mViewDataBinding
    }

    /**
     * To initialise live data observables
     */
    abstract fun initLiveDataObservables()

    /**
     * To initialise data
     */
    abstract fun initData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        initLiveDataObservables()
        initData()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    /**
     * To perform data binding operation
     */
    private fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.mViewModel = if (!::mViewModel.isInitialized) getViewModel() else mViewModel
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding.executePendingBindings()
        mViewModel.getErrorMessage().observe(this, errorMessageObserver)
    }

    /**
     * To load progress dialog on screen
     */
    fun showLoading() {
        if (mProgressDialog == null) {
            mProgressDialog = DolphinProgressDialog(this@BaseActivity)
        }
        mProgressDialog!!.setCancelable(false)
        mProgressDialog!!.show()
    }

    /**
     * To hide showing dialog
     */
    fun hideLoading() {
        if (mProgressDialog != null)
            mProgressDialog!!.let {
                if (it != null && it.isShowing)
                    it.cancel()
            }
    }

    /**
     * To handle error
     */
    private val errorMessageObserver: Observer<String> = Observer { t ->
        Logger.getLogger(BaseFragment::class.java.name).warning(t.toString())
        showMessage(t.toString())
    }

    /**
     * To check permission granted or not
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * To request permission
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    /**
     * To hide keyboard from screen
     */
    fun hideKeyboard() {
        val view = this.currentFocus
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    /**
     * To hide keyboard from screen
     */
    fun showKeyboard(view: View) {
        val view = this.currentFocus
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    /**
     * Fragment transaction.
     *
     * @param container        the container
     * @param transactionType  the transaction type
     * @param fragment         the mNewListFragment
     * @param isAddToBackStack the is ic_add_to_cart to ic_back stack
     * @param tag              the tag
     * @param enterAnim        Id of enter animation
     * @param exitAnim         Id of exit animation
     * @param pushAnim         Id of push animation
     * @param popAnim          Id of pop animation
     */
    fun fragmentTransaction(
        container: Int, transactionType: Int, fragment: Fragment, isAddToBackStack: Boolean,
        tag: String, enterAnim: Int, exitAnim: Int, pushAnim: Int, popAnim: Int,
        view: View?, isSharedElementAvailable: Boolean
    ) {

        val trans = supportFragmentManager.beginTransaction()
        trans.setCustomAnimations(enterAnim, exitAnim, pushAnim, popAnim)

        if (isSharedElementAvailable) {
            val enterTransitionSet = TransitionSet()
            enterTransitionSet.addTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.move))
            enterTransitionSet.duration = 400
            fragment.sharedElementEnterTransition = enterTransitionSet

            trans.addSharedElement(view!!, ViewCompat.getTransitionName(view)!!)
        }

        when (transactionType) {
            ADD_FRAGMENT -> {
                trans.add(container, fragment, tag)
                if (isAddToBackStack)
                    trans.addToBackStack(tag)
            }
            REMOVE_FRAGMENT -> {
                trans.remove(fragment)
                supportFragmentManager.popBackStack()
            }
            REPLACE_FRAGMENT -> {
                trans.replace(container, fragment, tag)
                if (isAddToBackStack)
                    trans.addToBackStack(tag)
            }
        }

        trans.commit()
    }

    /**
     * To display message
     */
    fun showMessage(message: String) {
        if (message.equals("Token expired", FLAG_TRUE)) {
            PreferenceManager.getInstance().clearLocalData()
        } else {
            showSnackbar(message)
        }
    }

    /**
     * Shows a [Snackbar].
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    protected fun showSnackbar(mainTextStringId: Int, actionStringId: Int, listener: View.OnClickListener) {
        Snackbar.make(findViewById(android.R.id.content), getString(mainTextStringId), Snackbar.LENGTH_INDEFINITE)
            .setAction(getString(actionStringId), listener).show()
    }

    /**
     * Shows a [Snackbar] using `text`.
     *
     * @param text The Snackbar text.
     */
    private fun showSnackbar(text: String) {
        val container = findViewById<View>(android.R.id.content)
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show()
        }
    }

    /**
     *Enable location provider.
     * */
    fun checkLocationIsEnableOrNot() {
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gpsEnabled = false
        var networkEnabled = false
        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
        }

        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }


        if (!gpsEnabled && !networkEnabled) {
            val dialog = android.app.AlertDialog.Builder(this@BaseActivity)
            dialog.setMessage(resources.getString(R.string.gps_network_not_enabled))
            dialog.setPositiveButton(resources.getString(R.string.opn_setting)) { _, _ ->
                callSetting()
            }

            dialog.setNegativeButton(getString(R.string.cancel)) { _, _ ->
                fragmentManager?.popBackStack()
            }

            dialog.show()
        } else {
            if (PermissionManager.hasLocationPermission()) {
                initLocation()
            } else {
                getPermission(
                    object : PermissionCallback {
                        override fun onPermissionResult(requestCode: Int, granted: Boolean) {
                            if (granted) {
                                initLocation()
                            }
                        }

                    }, LOCATION_ENABLE_CALLBACK,
                    FLAG_TRUE, FLAG_FALSE, FLAG_FALSE
                )
            }
        }
    }

    /**
     * get permission
     * @param listener     permission call back listener.
     * @param requestCode  request code for permission
     * @param location     has location permission.
     * @param storage      has storage permission.
     * @param camera       has Camera Permission.
     *
     * */
    private fun getPermission(
        listener: PermissionCallback, requestCode: Int, location: Boolean, storage: Boolean, camera: Boolean
    ) {

        this.listener = listener

        if (PermissionManager.askForPermission()) {

            val listPermissions = ArrayList<String>()

            // Request location permission
            if (location && !PermissionManager.hasLocationPermission()) {
                listPermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
                listPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }

            // Request Storage permission
            if (storage && !PermissionManager.hasStoragePermission()) {
                listPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
                listPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }

            //Camera Permission
            if (camera && !PermissionManager.hasCameraPermission()) {
                listPermissions.add(Manifest.permission.CAMERA)
            }

            if (listPermissions.size > 0) {

                val permissions = listPermissions.toTypedArray()

                ActivityCompat.requestPermissions(this, permissions, requestCode)
            } else {

                listener.onPermissionResult(requestCode, true)
            }

        } else {

            listener.onPermissionResult(requestCode, true)
        }
    }

    fun initLocation() {
        val INTERVAL = (1000 * 500).toLong()
        val FASTEST_INTERVAL = (1000 * 300).toLong()
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = INTERVAL // two minute interval
        mLocationRequest!!.fastestInterval = FASTEST_INTERVAL
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mFusedLocationClient!!.requestLocationUpdates(mLocationRequest, object : LocationCallback() {
                    override fun onLocationResult(p0: LocationResult?) {
                        for (location in p0!!.locations) {
                            onLocationChanged(location)
                        }
                    }
                }, Looper.myLooper())
            }
        } else {
            mFusedLocationClient!!.requestLocationUpdates(mLocationRequest, object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult?) {
                    for (location in p0!!.locations) {
                        onLocationChanged(location)
                    }
                }
            }, Looper.myLooper())
        }
    }

    override fun onLocationChanged(location: Location?) {
        DolphinMobilityApp.currentLocation = location
    }

    /**
     * Launch setting intent.
     * */
    private fun callSetting() {
        val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivityForResult(myIntent, LOCATION_ENABLE_CALLBACK)
    }
}