package com.dolphin.mobility.base.utils

import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import com.google.android.material.bottomnavigation.BottomNavigationView

/**
 * Created by Dhaval Parmar on 2019-05-18.
 * Email : dhvlparmar7@gmail.com
 */
@BindingMethods(
    BindingMethod(
        type = BottomNavigationView::class,
        attribute = "app:onNavigationItemSelected",
        method = "setOnNavigationItemSelectedListener"
    )
)
class MethodBindingAdapter