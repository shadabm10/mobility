package com.dolphin.mobility.base.network

import com.dolphin.mobility.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
object NetworkApiClient {
    private var mNetworkApiServices: NetworkApiService? = null

    private var httpClient: OkHttpClient.Builder? = null
    /**
     * To build retrofit object
     */
    private fun getRetrofitClient() {
        httpClient = OkHttpClient.Builder().apply {
            //            addNetworkInterceptor(StethoInterceptor())
            addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            })
            readTimeout(5000, TimeUnit.SECONDS)
            writeTimeout(5000, TimeUnit.SECONDS)
            connectTimeout(5000, TimeUnit.SECONDS)
        }

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient!!.build())
            .build()

        mNetworkApiServices = retrofit.create(NetworkApiService::class.java)
    }

    /**
     * To connect network service
     */
    fun getNetworkServices(): NetworkApiService {
        return if (mNetworkApiServices != null) {
            mNetworkApiServices as NetworkApiService
        } else {
            getRetrofitClient()
            mNetworkApiServices as NetworkApiService
        }
    }
}