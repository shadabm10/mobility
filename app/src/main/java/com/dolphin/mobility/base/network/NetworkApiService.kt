package com.dolphin.mobility.base.network

import com.dolphin.mobility.base.network.NetworkApiConstants.DIRECTION
import com.dolphin.mobility.base.network.NetworkApiConstants.FETCH_ADS
import com.dolphin.mobility.base.network.NetworkApiConstants.GET_AVAILABLE_COUPONS
import com.dolphin.mobility.base.network.NetworkApiConstants.GET_COUPON
import com.dolphin.mobility.base.network.NetworkApiConstants.GET_HELP
import com.dolphin.mobility.base.network.NetworkApiConstants.GET_SUPPORT
import com.dolphin.mobility.base.network.NetworkApiConstants.PLACE_DETAILS
import com.dolphin.mobility.base.network.NetworkApiConstants.POST_DEVICE
import com.dolphin.mobility.base.network.NetworkApiConstants.REQUEST_OTP
import com.dolphin.mobility.base.network.NetworkApiConstants.SEARCH_PLACES
import com.dolphin.mobility.base.network.NetworkApiConstants.VERIFY_OTP
import com.dolphin.mobility.data.request.*
import com.dolphin.mobility.data.response.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
interface NetworkApiService {

    @POST(REQUEST_OTP)
    fun requestOtp(@Body requestOtpRequest: RequestOtpRequest): Observable<RequestOtpResponse>

    @POST(VERIFY_OTP)
    fun verifyOtp(@Body verifyOtpRequest: VerifyOtpRequest): Observable<Response<Any>>

    @POST(FETCH_ADS)
    fun fetchAds(
        @Header("Authorization") auth: String,
        @Body request: AdsRequest
    ): Observable<AdsResponse>

    @GET(SEARCH_PLACES)
    fun getPlaces(
        @Query("input") input: String,
        @Query("components") components: String,
//                    @Query("location") location: String,
//                    @Query("radius") radius: String,
//                    @Query("strictbounds") strictbounds: String,
        @Query("language") language: String,
        @Query("key") key: String
    ): Observable<SearchLocation>

    @GET(PLACE_DETAILS)
    fun getPlaceDetailsById(
        @Query("placeid") location: String,
        @Query("language") language: String,
        @Query("key") key: String
    ): Observable<PlaceDetailResponse>

    @GET(DIRECTION)
    fun fetchPath(
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("language") language: String,
        @Query("key") key: String
    ): Observable<DirectionData>

//    @Multipart
//    @POST(REGISTER_DEVICE)
//    fun registerDevice(
//        @Part deviceId: MultipartBody.Part,
//        @Part fcmToken: MultipartBody.Part
//    ): Observable<Response<Any>>

    @POST(POST_DEVICE)
    fun registerDevice(
        @Header("Authorization") auth: String,
        @Body request: PostDeviceRequest
    ): Observable<Response<Any>>

    @GET(GET_SUPPORT)
    fun fetchSupportDetails(@Header("Authorization") auth: String): Observable<GetSupportResponse>

    @GET(GET_AVAILABLE_COUPONS)
    fun fetchAvailableCoupons(@Header("Authorization") auth: String): Observable<GetCouponListResponse>

    @GET(GET_HELP)
    fun fetchHelpDetails(@Header("Authorization") auth: String): Observable<HelpDataResponse>

    @POST(GET_COUPON)
    fun getCouponForUser(
        @Header("Authorization") auth: String,
        @Body request: GetCouponRequest
    ): Observable<CommonResponse>
}