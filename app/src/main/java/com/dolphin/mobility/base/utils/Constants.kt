package com.dolphin.mobility.base.utils

import android.os.Build
import android.util.Log
import java.util.*

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
object Constants {
    const val ADD_FRAGMENT = 1
    const val REMOVE_FRAGMENT = 2
    const val REPLACE_FRAGMENT = 3

    const val FLAG_TRUE = true
    const val FLAG_FALSE = false

    // Navigations
    const val OPEN_AUTHENTICATION_ACTIVITY = 1
    const val SHOW_OTP_DIALOG = 2
    const val OPEN_DASHBOARD_ACTICITY = 3
    const val OPEN_HELP_DIALOG = 4
    const val OPEN_COUPON_DIALOG = 5
    const val OPEN_VOLUME_DIALOG = 6
    const val OPEN_LOCATION_SCREEN = 7
    const val OPEN_SUPPORT_DIALOG = 8

    // HELP DATA TYPE
    const val TEXT_DATA = 1
    const val FORM_DATA = 2

    const val LOCATION_ENABLE_CALLBACK: Int = 5001

    const val GOOGLE_API_KEY_FOR_PLACES = "AIzaSyAHxUVkBQVMGVFNEDJMuPhWLx9lXr50UY8"

    // GENDER CONSTANTS
    const val MALE = "MALE"
    const val FEMALE = "FEMALE"

    // BUNDLE ARGUMENT KEYS
    const val REQUESTED_OTP_DATA = "requested_otp_data"
    const val COUPON_CODE = "coupon_code"
    const val COUPON_DETAILS = "coupon_details"

    // SHared Preference Constants
    const val IS_LOGGED_IN = "is_logged_in"
    const val DEVICE_TOKEN = "device_token"
    const val USER_NAME = "user_name"
    const val AUTH_TOKEN = "auth_token"
    const val VEHICLE_NUMBER = "vehicle_number"

    // Push Notification Received Data Constants
    const val NEW_AGE = "age"
    const val NEW_GENDER = "gender"

    // SOCKET Constant
    const val SOCKET_CONNECTION_URL = "https://still-atoll-42300.herokuapp.com/"
    const val REFRESH_PLAYLIST = "refresh_playlist"

    /**
     * Mehtod to get device's unique device id
     *
     * @return unique device id
     */
    fun getUniqueIdOfDevice(): String? {
        var deviceId = ""
        var serial = ""

        val m_szDevIDShort = ("35" + Build.BOARD.length % 10
                + Build.BRAND.length % 10 + Build.CPU_ABI.length % 10
                + Build.DEVICE.length % 10
                + Build.MANUFACTURER.length % 10
                + Build.MODEL.length % 10 + Build.PRODUCT.length % 10)


        try {
            serial = android.os.Build::class.java.getField("SERIAL").get(null)
                .toString()
            Log.e("unique-1", serial)
            Log.e("unique-2", m_szDevIDShort)
            // Go ahead and return the serial for api => 9
            return UUID(m_szDevIDShort.hashCode().toLong(), serial.hashCode().toLong())
                .toString()
        } catch (e: Exception) {
            // String needs to be initialized
            serial = "serial" // some value
        }

        deviceId = UUID(m_szDevIDShort.hashCode().toLong(), serial!!.hashCode().toLong()).toString()
        return deviceId
    }
}