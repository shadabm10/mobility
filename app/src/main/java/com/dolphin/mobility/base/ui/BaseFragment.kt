package com.dolphin.mobility.base.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.base.viewmodel.BaseViewModel
import java.util.logging.Logger

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {
    protected lateinit var mViewDataBinding: T
    protected lateinit var mViewModel: V
    protected var mContentView: View? = null
    protected var mActivity: BaseActivity<*, *>? = null

    /**
     * To get layout file
     */
    protected abstract fun getLayout(): Int

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun getBindingVariable(): Int


    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): V

    /**
     * To initialise data
     */
    abstract fun initData()

    /**
     * To initialise live data observables
     */
    abstract fun initLiveDataObservables()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = getViewModel()
    }

    /**
     * To handle progress
     */
    private val progressObserver: Observer<Boolean> = Observer {
        if (it!!)
            (activity as BaseActivity<*, *>).showLoading()
        else
            (activity as BaseActivity<*, *>).hideLoading()
    }

    override fun onDestroy() {
        super.onDestroy()
        mContentView = null
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayout(), container, FLAG_FALSE)
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding.executePendingBindings()
        if (mContentView == null) {
            mContentView = mViewDataBinding.root
            mViewModel.getErrorMessage().observe(this, errorMessageObserver)
            mViewModel.getProgress().observe(this, progressObserver)
        }
        return mContentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initLiveDataObservables()
    }

    /**
     * To handle error
     */
    private val errorMessageObserver: Observer<String> = Observer<String> { t ->
        (activity as BaseActivity<*, *>).hideLoading()
        Logger.getLogger(BaseFragment::class.java.name).warning(t.toString())
        (activity as BaseActivity<*, *>).showMessage(t.toString())
    }

}