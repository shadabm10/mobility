package com.dolphin.mobility.base.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import com.dolphin.mobility.R

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class DolphinProgressDialog(context: Context?) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.layout_loading_dialog)
    }
}