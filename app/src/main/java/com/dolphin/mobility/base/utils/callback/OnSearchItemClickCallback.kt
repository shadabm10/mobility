package com.dolphin.mobility.base.utils.callback

/**
 * Created by Dhaval Parmar on 2019-05-22.
 * Email : dhvlparmar7@gmail.com
 */
interface OnSearchLocationItemClickCallback {
    fun onSearchLocation(placeId: String, placeName: String)
}