package com.dolphin.mobility.base.utils

import android.content.SharedPreferences
import com.dolphin.mobility.DolphinMobilityApp.Companion.sharedPreferences

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class PreferenceManager {

    companion object {
        private var instance: PreferenceManager? = null

        @Synchronized
        fun getInstance(): PreferenceManager {
            if (instance == null) {
                instance = PreferenceManager()
            }

            return instance as PreferenceManager
        }
    }

    /**
     * Save values in shared preference.
     */

    fun save(key: String, value: Any?) {
        val editor = getEditor()
        when {
            value is Boolean -> editor.putBoolean(key, (value as Boolean?)!!)
            value is Int -> editor.putInt(key, (value as Int?)!!)
            value is Float -> editor.putFloat(key, (value as Float?)!!)
            value is Long -> editor.putLong(key, (value as Long?)!!)
            value is String -> editor.putString(key, value as String?)
            value is Enum<*> -> editor.putString(key, value.toString())
            value is Double -> editor.putString(key, value.toString())
            value != null -> throw RuntimeException("Attempting to save non-supported preference")
        }

        editor.commit()
    }

    /**
     * Get shared preference editor.
     * */
    private fun getEditor(): SharedPreferences.Editor {
        return sharedPreferences.edit()
    }

    /**
     * Clear Shared Preference
     */
    fun clearLocalData() {
        sharedPreferences.edit().clear().apply()
    }

    /**
     *  Get value from shared preference with default value.
     * */
    @Suppress("UNCHECKED_CAST")
    operator fun <T> get(key: String, defValue: T): T {
        val returnValue = sharedPreferences.all[key] as T
        return returnValue ?: defValue
    }

    /**
     * Get value from shared preference(without default value).
     */
    @Suppress("UNCHECKED_CAST")
    operator fun <T> get(key: String): T {
        return sharedPreferences.all[key] as T
    }
}