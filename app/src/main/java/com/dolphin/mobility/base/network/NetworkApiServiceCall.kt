package com.dolphin.mobility.base.network

import com.dolphin.mobility.base.utils.Constants
import com.dolphin.mobility.base.utils.Constants.AUTH_TOKEN
import com.dolphin.mobility.base.utils.Constants.GOOGLE_API_KEY_FOR_PLACES
import com.dolphin.mobility.base.utils.Constants.VEHICLE_NUMBER
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.data.request.*
import com.dolphin.mobility.data.response.VerifyOtpResponse
import com.dolphin.mobility.ui.auth.viewmodel.AuthenticationViewModel
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import com.dolphin.mobility.ui.dashboard.viewmodel.LocationViewModel
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.ConnectException
import java.util.concurrent.TimeUnit

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class NetworkApiServiceCall(networkApiServices: NetworkApiService) {
    private var networkApiServices: NetworkApiService? = null

    init {
        this.networkApiServices = networkApiServices
    }

    fun fetchAds(request: AdsRequest, viewModel: DashboardViewModel): Disposable? {
        return networkApiServices?.fetchAds(PreferenceManager.getInstance()[AUTH_TOKEN], request)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                viewModel.onSuccessfullyFetchAds(it)
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    fun searchPlace(query: String?, locationViewModel: LocationViewModel): Disposable? {
        return networkApiServices?.getPlaces(
            query!!,
            "country:us|country:in",
//            "${MustApp.currentLocation!!.latitude},${MustApp.currentLocation!!.longitude}",
//            "5000",
            "en",
            GOOGLE_API_KEY_FOR_PLACES
        )
            ?.debounce(1500, TimeUnit.MILLISECONDS)
            ?.distinctUntilChanged()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                locationViewModel.onSuccessGetLocation(it)
            }, {

            })
    }

    /**
     *Google place details by ID
     */
    fun getPlaceDetailsById(placeId: String, viewModel: LocationViewModel): Disposable? {
        return networkApiServices?.getPlaceDetailsById(placeId, "en", GOOGLE_API_KEY_FOR_PLACES)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                viewModel.onSuccessFetchPlaceDetails(it)
            }, { t ->
                if (t is HttpException) {
                    viewModel.handleApiErrors(t.message.toString())
                } else if (t is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    /**
     * For Fetch path between two locations
     */
    fun fetchPath(origin: String, destination: String, viewModel: LocationViewModel): Disposable? {
        return networkApiServices?.fetchPath(origin, destination, "en", GOOGLE_API_KEY_FOR_PLACES)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(Schedulers.io())
            ?.subscribe({ t ->
                if (t.success == "OK")
                    viewModel.onFetchPath(t)
                else
                    viewModel.onFetchPath(t)
            }, { t ->
                if (t is HttpException) {
                    viewModel.handleApiErrors(t.message.toString())
                } else if (t is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    /**
     * For Requesting OTP for authenticate the user to logged in inside application
     */
    fun requestOtp(email: String, viewModel: AuthenticationViewModel): Disposable? {
        return networkApiServices?.requestOtp(RequestOtpRequest(email))
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                viewModel.onReceiveOtp(it)
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    /**
     * For Verify OTP for authenticate the user to logged in inside application
     */
    fun verifyOtp(verifyOtpRequest: VerifyOtpRequest, viewModel: AuthenticationViewModel): Disposable? {
        return networkApiServices?.verifyOtp(verifyOtpRequest)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                PreferenceManager.getInstance().save(AUTH_TOKEN, it.headers().get("Authorization"))
                viewModel.onVerifyOtp(Gson().fromJson(Gson().toJson(it.body()), VerifyOtpResponse::class.java))
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    /**
     * For updating device id and device token(FCM TOKEN)
     */
    fun registerDevice(viewModel: DashboardViewModel): Disposable? {
        val deviceId = Constants.getUniqueIdOfDevice()!!
//        val deviceIdPart: MultipartBody.Part = MultipartBody.Part.createFormData("device_id", deviceId)
//        val fcmToken: String = PreferenceManager.getInstance()[Constants.DEVICE_TOKEN]
//        val fcmTokenPart: MultipartBody.Part = MultipartBody.Part.createFormData("fcm_token", fcmToken)
        return networkApiServices?.registerDevice(
            PreferenceManager.getInstance()[AUTH_TOKEN],
            PostDeviceRequest(deviceId, PreferenceManager.getInstance()[VEHICLE_NUMBER, ""])
        )
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    fun fetchSupportDetails(viewModel: DashboardViewModel): Disposable? {
        return networkApiServices?.fetchSupportDetails(PreferenceManager.getInstance()[AUTH_TOKEN])
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                viewModel.onSuccessFullyFetchSupportData(it.data)
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    fun fetchAvailableCoupons(viewModel: DashboardViewModel): Disposable? {
        return networkApiServices?.fetchAvailableCoupons(PreferenceManager.getInstance()[AUTH_TOKEN])
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                viewModel.onSuccessFullyFetchAvailableCouponsData(it.data)
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    fun fetchHelpDetails(viewModel: DashboardViewModel): Disposable? {
        return networkApiServices?.fetchHelpDetails(PreferenceManager.getInstance()[AUTH_TOKEN])
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                if (it.statusCode == 200) {
                    viewModel.onSuccessFullyFetchHelpData(it.data)
                }
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }

    fun getCouponForUser(request: GetCouponRequest, viewModel: DashboardViewModel): Disposable? {
        return networkApiServices?.getCouponForUser(PreferenceManager.getInstance()[AUTH_TOKEN], request)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                viewModel.onSuccessFullyredeemCouponData(it)
            }, {
                if (it is HttpException) {
                    viewModel.handleApiErrors(it.message())
                } else if (it is ConnectException) {
                    viewModel.handleApiErrors("Check Internet Connection")
                }
            })
    }
}