package com.dolphin.mobility.services

import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dolphin.mobility.base.utils.Constants.DEVICE_TOKEN
import com.dolphin.mobility.base.utils.Constants.NEW_AGE
import com.dolphin.mobility.base.utils.Constants.NEW_GENDER
import com.dolphin.mobility.base.utils.PreferenceManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject

/**
 * Created by Dhaval Parmar on 2019-07-17.
 * Email : dhvlparmar7@gmail.com
 */
class PushService : FirebaseMessagingService() {

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        PreferenceManager.getInstance().save(DEVICE_TOKEN, token)
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        val dataMap = message?.data
        val data = JSONObject(dataMap)

        if (data.has("age") && data.has("gender")) {
            val age = data.getInt("age")
            val gender = data.getString("gender")
            Log.e("MESSAGE RECEIVED", "$age / $gender")
            val intent = Intent("ACTION_REFRESH_PLAYLIST")
            intent.putExtra(NEW_AGE, age)
            intent.putExtra(NEW_GENDER, gender)
            LocalBroadcastManager.getInstance(this@PushService).sendBroadcast(intent)
        }
    }
}