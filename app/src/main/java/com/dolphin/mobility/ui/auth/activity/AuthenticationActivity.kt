package com.dolphin.mobility.ui.auth.activity

import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseActivity
import com.dolphin.mobility.base.utils.Constants.ADD_FRAGMENT
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.base.utils.Constants.FLAG_TRUE
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.databinding.ActivityAuthenticationBinding
import com.dolphin.mobility.ui.auth.fragment.LoginFragment
import com.dolphin.mobility.ui.auth.viewmodel.AuthenticationViewModel

/**
 * Created by Dhaval Parmar on 22/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class AuthenticationActivity :
    BaseActivity<ActivityAuthenticationBinding, AuthenticationViewModel>() {

    override fun getLayoutId(): Int = R.layout.activity_authentication

    override fun getViewModel(): AuthenticationViewModel = ViewModelProviders
        .of(this@AuthenticationActivity)
        .get(AuthenticationViewModel::class.java)

    override fun getBindingVariable(): Int = BR._all

    override fun initLiveDataObservables() {
        PreferenceManager.getInstance().clearLocalData()

    }

    override fun initData() {
        fragmentTransaction(
            R.id.frame_container_authentication,
            ADD_FRAGMENT,
            LoginFragment(),
            FLAG_TRUE,
            LoginFragment::class.java.name,
            R.anim.anim_enter_right,
            R.anim.anim_still,
            R.anim.anim_still,
            R.anim.anim_exit_right,
            null,
            FLAG_FALSE
        )
    }
}