package com.dolphin.mobility.ui.dashboard.viewmodel

import android.app.Application
import android.util.Log
import android.view.MenuItem
import androidx.annotation.NonNull
import androidx.databinding.Bindable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crashlytics.android.Crashlytics
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.utils.Constants
import com.dolphin.mobility.base.utils.Constants.DEVICE_TOKEN
import com.dolphin.mobility.base.utils.Constants.OPEN_COUPON_DIALOG
import com.dolphin.mobility.base.utils.Constants.OPEN_HELP_DIALOG
import com.dolphin.mobility.base.utils.Constants.OPEN_LOCATION_SCREEN
import com.dolphin.mobility.base.utils.Constants.OPEN_SUPPORT_DIALOG
import com.dolphin.mobility.base.utils.Constants.OPEN_VOLUME_DIALOG
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.base.viewmodel.BaseViewModel
import com.dolphin.mobility.data.request.AdsRequest
import com.dolphin.mobility.data.request.GetCouponRequest
import com.dolphin.mobility.data.response.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import kotlin.random.Random


/**
 * Created by Dhaval Parmar on 2019-04-29.
 * Email : dhvlparmar7@gmail.com
 */
const val REFRESH_ADS_DELAY: Long = 180000

class DashboardViewModel(application: Application) : BaseViewModel(application),
    BottomNavigationView.OnNavigationItemSelectedListener {

    private val adsData = MutableLiveData<AdsResponse>()
    private val helpData = MutableLiveData<List<HelpData>>()
    private val supportData = MutableLiveData<List<SupportData>>()
    private val couponData = MutableLiveData<List<CouponData>>()
    private val redeemcouponData = MutableLiveData<BaseResponse>()

    @get:Bindable
    var age: String = ""
        set(age) {
            field = age
            notifyPropertyChanged(BR.age)
        }


    @get:Bindable
    var gender: String = ""
        set(gender) {
            field = gender
            notifyPropertyChanged(BR.gender)
        }

    @get:Bindable
    var name: String = ""
        set(name) {
            field = name
            notifyPropertyChanged(BR.name)
        }

    @get:Bindable
    var email: String = ""
        set(email) {
            field = email
            notifyPropertyChanged(BR.email)
        }
    @get:Bindable
    var phone: String = ""
        set(phone) {
            field = phone
            notifyPropertyChanged(BR.phone)
        }

    @get:Bindable
    var couponCode: String = ""
        set(couponCode) {
            field = couponCode
            notifyPropertyChanged(BR.couponCode)
        }

    /**
     * Register / Post Device Info
     */
    fun postDevice() {
        mDisposable = mNetworkClient.registerDevice(this@DashboardViewModel)!!
    }

    /**
     * Fetch The Ads for Dashboard
     */
    fun fetchAds(age: Int, gender: String) {
        mDisposable = mNetworkClient.fetchAds(AdsRequest(age = age, gender = gender), this@DashboardViewModel)!!
    }

    /**
     * Get the ads live data to observe
     */
    fun getAdsData(): LiveData<AdsResponse> {
        return adsData
    }

    /**
     * On Success of fetching the ads
     */
    fun onSuccessfullyFetchAds(result: AdsResponse) {
        if (result.playList.isNotEmpty()) {
            adsData.postValue(result)
            age = "Showing Ads for Age ${result.filterCriteria.age}"
            gender = "Showing Ads for Gender ${result.filterCriteria.gender}"
            super.handleApiErrors("Showing Ads for Age ${result.filterCriteria.age} and Gender ${result.filterCriteria.gender}")
//            Handler().postDelayed({
//                val randomAgeLimit = Random.nextInt(1, 100)
//                val leftOver = randomAgeLimit % 5
//                val randomAge = randomAgeLimit + leftOver
//                val randomGenderLimit = Random.nextBoolean()
//                val randomGender = if (randomGenderLimit) Constants.MALE else Constants.FEMALE
//                fetchAds(randomAge, randomGender)
//            }, REFRESH_ADS_DELAY)
        } else {
            val randomAgeLimit = Random.nextInt(1, 100)
            val leftOver = randomAgeLimit % 5
            val randomAge = randomAgeLimit + leftOver
            val randomGenderLimit = Random.nextBoolean()
            val randomGender = if (randomGenderLimit) Constants.MALE else Constants.FEMALE
            fetchAds(randomAge, randomGender)
        }
    }

    /**
     * Fetch Help Data
     */
    fun fetchHelpDetails() {
        mDisposable = mNetworkClient.fetchHelpDetails(this@DashboardViewModel)!!
    }

    /**
     * Get the ads live data to observe
     */
    fun getHelpData(): LiveData<List<HelpData>> {
        return helpData
    }

    /**
     * On Success of fetching the help data
     */
    fun onSuccessFullyFetchHelpData(data: List<HelpData>) {
        helpData.postValue(data)
    }

    /**
     * Fetch Help Data
     */
    fun fetchSupportDetails() {
        mDisposable = mNetworkClient.fetchSupportDetails(this@DashboardViewModel)!!
    }

    /**
     * Get the ads live data to observe
     */
    fun getSupportData(): LiveData<List<SupportData>> {
        return supportData
    }

    /**
     * On Success of fetching the help data
     */
    fun onSuccessFullyFetchSupportData(data: List<SupportData>) {
        supportData.postValue(data)
    }

    /**
     * Fetch Help Data
     */
    fun fetchAvailableCoupons() {
        mDisposable = mNetworkClient.fetchAvailableCoupons(this@DashboardViewModel)!!
    }

    /**
     * Get the ads live data to observe
     */
    fun getCouponData(): LiveData<List<CouponData>> {
        return couponData
    }

    /**
     * On Success of fetching the help data
     */
    fun onSuccessFullyFetchAvailableCouponsData(data: List<CouponData>) {
        couponData.postValue(data)
    }

    /**
     * Fetch Help Data
     */
    fun redeemCoupon() {
        val request = GetCouponRequest(couponCode, email, name, phone)
        mDisposable = mNetworkClient.getCouponForUser(request, this@DashboardViewModel)!!
    }

    /**
     * Get the ads live data to observe
     */
    fun getredeemCouponData(): LiveData<BaseResponse> {
        return redeemcouponData
    }

    /**
     * On Success of fetching the help data
     */
    fun onSuccessFullyredeemCouponData(data: BaseResponse) {
        redeemcouponData.postValue(data)
    }

    override fun handleApiErrors(message: String) {
        super.handleApiErrors(message)
        val randomAgeLimit = Random.nextInt(1, 100)
        val leftOver = randomAgeLimit % 5
        val randomAge = randomAgeLimit + leftOver
        val randomGenderLimit = Random.nextBoolean()
        val randomGender = if (randomGenderLimit) Constants.MALE else Constants.FEMALE
        fetchAds(randomAge, randomGender)
    }

    /**
     * Selection of bottom menu
     */
    override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_help -> pageNavigation.postValue(OPEN_HELP_DIALOG)
            R.id.action_coupon -> pageNavigation.postValue(OPEN_COUPON_DIALOG)
            R.id.action_volume -> pageNavigation.postValue(OPEN_VOLUME_DIALOG)
            R.id.action_location -> pageNavigation.postValue(OPEN_LOCATION_SCREEN)
            R.id.action_support -> pageNavigation.postValue(OPEN_SUPPORT_DIALOG)
        }
        return false
    }

    /**
     * To generate Firebase Instance Id
     */
    fun generateFirebaseInstanceId() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Crashlytics.logException(task.exception)
                    Crashlytics.log(task.exception!!.localizedMessage)
                    Crashlytics.log("FCM NOT GENERATING...")
                    Log.w("INSTANCE ID", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                PreferenceManager.getInstance().save(DEVICE_TOKEN, token)
                mDisposable = mNetworkClient.registerDevice(this@DashboardViewModel)!!
                Crashlytics.log("GENERATED FCM $token")
                // Log and toast
                Log.e("INSTANCE ID", token)
            })
    }
}