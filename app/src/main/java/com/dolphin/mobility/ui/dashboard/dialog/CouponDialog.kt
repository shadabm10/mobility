package com.dolphin.mobility.ui.dashboard.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseDialogFragment
import com.dolphin.mobility.base.utils.Constants.COUPON_CODE
import com.dolphin.mobility.base.utils.Constants.COUPON_DETAILS
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.custom.DashedDividerItemDecoration
import com.dolphin.mobility.data.response.CouponData
import com.dolphin.mobility.databinding.DialogCouponBinding
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import kotlinx.android.synthetic.main.dialog_coupon.*
import kotlinx.android.synthetic.main.item_coupon.view.*


/**
 * Created by Dhaval Parmar on 23/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class CouponDialog : BaseDialogFragment<DialogCouponBinding, DashboardViewModel>() {

    override fun getLayout(): Int = R.layout.dialog_coupon

    override fun getBindingVariable(): Int = BR.couponViewModel

    override fun getViewModel(): DashboardViewModel = ViewModelProviders
        .of(this@CouponDialog)
        .get(DashboardViewModel::class.java)


    override fun initData() {
        mViewModel.fetchAvailableCoupons()
        recycler_coupon.addItemDecoration(
            DashedDividerItemDecoration(
                context?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.dashed_divider
                    )
                }!!
            )
        )
    }

    override fun initLiveDataObservables() {
        mViewModel.getCouponData().observe(this@CouponDialog, availableCouponObserver)
    }

    private val redeemCouponObserver = Observer<CouponData> {
        val redeemCouponDialog = RedeemCouponDialog()
        val mBundle = Bundle()
        mBundle.putString(COUPON_CODE, it.code)
        mBundle.putString(COUPON_DETAILS, "${it.category} : ${it.description}")
        redeemCouponDialog.arguments = mBundle
        redeemCouponDialog.show(fragmentManager!!, RedeemCouponDialog::class.java.name)
        dismiss()
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout((width * 0.75).toInt(), (height * 0.6).toInt())
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    activity as Context,
                    android.R.color.transparent
                )
            )
        )
    }

    private val availableCouponObserver = Observer<List<CouponData>> {
        recycler_coupon.adapter = CouponDataAdapter(it)
        (recycler_coupon.adapter as CouponDataAdapter).couponPosition.observe(this, redeemCouponObserver)
    }

    class CouponDataAdapter(private val mData: List<CouponData>) :
        RecyclerView.Adapter<CouponDataAdapter.CouponDataViewHolder>() {

        val couponPosition = MutableLiveData<CouponData>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CouponDataViewHolder {
            return CouponDataViewHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_coupon, parent, FLAG_FALSE)
            )
        }

        override fun getItemCount(): Int = mData.size

        override fun onBindViewHolder(holder: CouponDataViewHolder, position: Int) {
            holder.itemView.item_text_coupon_percent.text = "${mData[position].discountPercentage}%\nOFF"
            holder.itemView.item_text_coupon_title.text = "${mData[position].name} : ${mData[position].description}"
            holder.itemView.item_button_redeem_coupon.setOnClickListener {
                couponPosition.postValue(mData[position])
            }
        }

        inner class CouponDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    }

}