package com.dolphin.mobility.ui.dashboard.activity

import android.content.Intent
import android.view.LayoutInflater
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseActivity
import com.dolphin.mobility.base.utils.Constants.ADD_FRAGMENT
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.base.utils.Constants.FLAG_TRUE
import com.dolphin.mobility.base.utils.Constants.OPEN_COUPON_DIALOG
import com.dolphin.mobility.base.utils.Constants.OPEN_HELP_DIALOG
import com.dolphin.mobility.base.utils.Constants.OPEN_LOCATION_SCREEN
import com.dolphin.mobility.base.utils.Constants.OPEN_SUPPORT_DIALOG
import com.dolphin.mobility.base.utils.Constants.OPEN_VOLUME_DIALOG
import com.dolphin.mobility.base.utils.PermissionManager
import com.dolphin.mobility.base.utils.callback.PermissionCallback
import com.dolphin.mobility.databinding.ActivityDashboardBinding
import com.dolphin.mobility.ui.dashboard.dialog.CouponDialog
import com.dolphin.mobility.ui.dashboard.dialog.HelpDialog
import com.dolphin.mobility.ui.dashboard.dialog.SupportDialog
import com.dolphin.mobility.ui.dashboard.dialog.VolumeDialog
import com.dolphin.mobility.ui.dashboard.fragment.DashboardFragment
import com.dolphin.mobility.ui.dashboard.fragment.RouteLocationFragment
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import kotlinx.android.synthetic.main.activity_dashboard.*

import android.widget.ImageView
import android.widget.Toast
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.ui.auth.activity.AuthenticationActivity
import com.dolphin.mobility.ui.auth.fragment.LoginFragment
import kotlin.system.exitProcess
import android.app.ActivityManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import androidx.annotation.RequiresApi


/**
 * Created by Dhaval Parmar on 2019-04-29.
 * Email : dhvlparmar7@gmail.com
 */
class DashboardActivity : BaseActivity<ActivityDashboardBinding, DashboardViewModel>(), PermissionCallback {

    private lateinit var myFragment: DashboardFragment


    override fun getLayoutId(): Int = R.layout.activity_dashboard

    override fun getViewModel(): DashboardViewModel = ViewModelProviders
        .of(this@DashboardActivity)
        .get(DashboardViewModel::class.java)

    override fun getBindingVariable(): Int = BR.dashBoardViewModel

    override fun initLiveDataObservables() {
        mViewModel.getPageRedirection().observe(this, navigateObserver)
    }

    override fun initData() {

        fragmentTransaction(
            R.id.container,
            ADD_FRAGMENT,
            DashboardFragment(),
            FLAG_TRUE,
            DashboardFragment::class.java.name,
            R.anim.anim_enter_right,
            R.anim.anim_still,
            R.anim.anim_still,
            R.anim.anim_exit_right,
            null,
            FLAG_FALSE
        )

        val bottomNavigationMenuView = navigation.getChildAt(0) as BottomNavigationMenuView
        val v = bottomNavigationMenuView.getChildAt(1)
        val itemView = v as BottomNavigationItemView

        val badge = LayoutInflater.from(this)
            .inflate(com.dolphin.mobility.R.layout.layout_notification_badge, itemView, true)

//        if (PreferenceManager.getInstance()[DEVICE_TOKEN, ""].isNullOrBlank())
        mViewModel.generateFirebaseInstanceId()

        val button = findViewById<ImageView>(R.id.image_log_out)
        button.setOnClickListener(){
            myFragment = DashboardFragment()
            myFragment.stopService()
            val intent = Intent(this@DashboardActivity, AuthenticationActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            PreferenceManager.getInstance().clearLocalData()



            startActivity(intent)
            KillBackgroundProcessesTask()
           // finishAffinity()
            exitProcess(0)


        }

    }
    @RequiresApi(Build.VERSION_CODES.CUPCAKE)
    private inner class KillBackgroundProcessesTask : AsyncTask<Void, Int, Int>() {
        override fun doInBackground(vararg Void: Void): Int? {
           // exitProcess(0)
            // Get an instance of PackageManager
            val pm = packageManager

            // Get an instance of ActivityManager
            val am = applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

            // Get a list of RunningAppProcessInfo
            val list = am.runningAppProcesses

            // Count the number of running processes
            val initialRunningProcessesSize = list.size

            // Iterate over the RunningAppProcess list
            for (process in list) {
                // Ignore, if the process contains package list is empty
                if (process.pkgList.size == 0) continue

                try {
                    // Get the PackageInfo for current process
                    val packageInfo = pm.getPackageInfo(process.pkgList[0], PackageManager.GET_ACTIVITIES)

                    // Ignore the self app package
                    if (packageInfo.packageName != applicationContext.packageName) {
                        // Try to kill other background processes
                        // System processes are ignored
                        am.killBackgroundProcesses(packageInfo.packageName)
                    }
                } catch (e: PackageManager.NameNotFoundException) {
                    // Catch the exception
                    e.printStackTrace()
                }

            }

            // Get the running processes after killing some
            val currentRunningProcessesSize = am.runningAppProcesses.size

            // Return the number of killed processes
            return initialRunningProcessesSize - currentRunningProcessesSize
        }
    }

    override fun onBackPressed() {
        // your stuff here

        super.onBackPressed()
        finish()
    }
    /**
     * To navigate to particular activity
     */
    private val navigateObserver = Observer<Int> { t ->
        when (t) {
            OPEN_HELP_DIALOG ->
                HelpDialog()
                    .show(supportFragmentManager, HelpDialog::class.java.name)
            OPEN_COUPON_DIALOG ->
                CouponDialog()
                    .show(supportFragmentManager, CouponDialog::class.java.name)
            OPEN_VOLUME_DIALOG ->
                VolumeDialog()
                    .show(supportFragmentManager, VolumeDialog::class.java.name)

            OPEN_LOCATION_SCREEN -> fragmentTransaction(
                R.id.container,
                ADD_FRAGMENT,
                RouteLocationFragment(),
                FLAG_TRUE,
                RouteLocationFragment::class.java.name,
                R.anim.anim_enter_right,
                R.anim.anim_still,
                R.anim.anim_still,
                R.anim.anim_exit_right,
                null,
                FLAG_FALSE
            )
            OPEN_SUPPORT_DIALOG ->
                SupportDialog()
                    .show(supportFragmentManager, SupportDialog::class.java.name)

        }
    }

    /**
     *  Permission callback
     * */
    override fun onPermissionResult(requestCode: Int, granted: Boolean) {
        if (PermissionManager.hasLocationPermission()) {
            initLocation()
        }
    }

    override fun onResume() {
        super.onResume()
        checkLocationIsEnableOrNot()


    }
}