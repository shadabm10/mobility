package com.dolphin.mobility.ui.dashboard.fragment
import android.app.AlertDialog
import android.content.*
import android.content.Context.AUDIO_SERVICE
import android.graphics.drawable.Animatable
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.session.PlaybackState
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.widget.*
import androidx.annotation.Nullable
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseFragment
import com.dolphin.mobility.base.utils.Constants.FLAG_TRUE
import com.dolphin.mobility.base.utils.Constants.MALE
import com.dolphin.mobility.base.utils.Constants.NEW_AGE
import com.dolphin.mobility.base.utils.Constants.NEW_GENDER
import com.dolphin.mobility.base.utils.Constants.REFRESH_PLAYLIST
import com.dolphin.mobility.base.utils.Constants.SOCKET_CONNECTION_URL
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.data.response.AdsResponse
import com.dolphin.mobility.data.response.Play
import com.dolphin.mobility.databinding.FragmentDashboardBinding
import com.dolphin.mobility.ui.auth.activity.AuthenticationActivity
import com.dolphin.mobility.ui.auth.viewmodel.RadioModel
import com.dolphin.mobility.ui.dashboard.activity.DashboardActivity
import com.dolphin.mobility.ui.dashboard.activity.RadioActivity
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import com.facebook.ads.*
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.imagepipeline.image.ImageInfo
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.upstream.cache.CacheDataSink
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.android.exoplayer2.util.Util
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.json.JSONObject
import java.io.File
import java.net.URISyntaxException
import java.util.*
/**
 * Created by Dhaval Parmar on 2019-04-29.
 * Email : dhvlparmar7@gmail.com
 */
const val IMAGE_AD_SHOW_TIME: Long = 10000
class DashboardFragment :
    BaseFragment<FragmentDashboardBinding, DashboardViewModel>() {
    private var adsCounter = 0
    private var adsList = mutableListOf<Play>()
    private var mVideoAdsPlayer: SimpleExoPlayer? = null
    private lateinit var videoCache: CacheDataSourceFactory
    private lateinit var playme: ImageButton
    private lateinit var stopme: ImageButton
    private lateinit var next: ImageButton
    private lateinit var previous: ImageButton
    private lateinit var title: TextView
    private lateinit var btnList: ImageButton
    private lateinit var btnSpeaker: ImageButton
     var mp = MediaPlayer()
    private lateinit var radioModelArrayList: ArrayList<RadioModel>
    private var index_pos = 0
    private var finishDialog: AlertDialog.Builder? = null
    private var st = "stopped"
    private var pl = "please wait ..."
    private var `in` = "note: it may take more seconds to buffer.Press menu for more information"
    private var nativeAd: NativeAd? = null
    private var adView: LinearLayout? = null
    private var nativeAdLayout: NativeAdLayout? = null
    private var intentFilter: IntentFilter
    private var updatePlaylistBroadcastReceiver: UpdatePlaylistBroadcastReceiver
    private lateinit var mSocket: Socket
    private var refreshPlaylistListener: Emitter.Listener
    private var mAudioManager: AudioManager? = null
    var url: String  = "Hello"


    init {
        updatePlaylistBroadcastReceiver = UpdatePlaylistBroadcastReceiver()
        intentFilter = IntentFilter()
        intentFilter.addAction("ACTION_REFRESH_PLAYLIST")
        try {
            mSocket = IO.socket(SOCKET_CONNECTION_URL)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }
        refreshPlaylistListener = Emitter.Listener { args ->
            val data = args[0] as JSONObject
            if (data.has("age") && data.has("gender")) {
                val age = data.getInt("age")
                val gender = data.getString("gender")
                Log.e("MESSAGE RECEIVED", "$age / $gender")
                mViewModel.fetchAds(age, gender)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAudioManager = this@DashboardFragment.context!!.getSystemService(AUDIO_SERVICE) as AudioManager
        val l1 = RadioActivity()
        l1.stopService()

        getRadio()
      //  Toast.makeText(context, " $`in`", Toast.LENGTH_SHORT).show()
        registerReceiver()
        mSocket.on(REFRESH_PLAYLIST, refreshPlaylistListener)
        mSocket.connect()
    }
    override fun getLayout(): Int = R.layout.fragment_dashboard
    override fun getBindingVariable(): Int = BR.dashBoardViewModel
    override fun getViewModel(): DashboardViewModel = activity?.let {
        ViewModelProviders
            .of(it)
            .get(DashboardViewModel::class.java)
    }!!
    override fun initData() {
        AdSettings.setIntegrationErrorMode(AdSettings.IntegrationErrorMode.INTEGRATION_ERROR_CRASH_DEBUG_MODE)
        AdSettings.addTestDevice("ae1c03a5-1b43-4b23-88c0-926761d8dca0")
        AdSettings.setDebugBuild(true)
        //  AdSettings.addTestDevice("1d4fd4a5-5279-49fe-9e16-1679d9abe5ad");
        // AdSettings.clearTestDevices();
        loadNativeAd()
        mViewModel.postDevice()
        mViewModel.fetchAds(-1, MALE)
        videoCache = CacheDataSourceFactory(activity as Context, 250 * 1024 * 1024, 50 * 1024 * 1024)
    }
    override fun initLiveDataObservables() {
        mViewModel.getAdsData().observe(this@DashboardFragment, adsObserver)
    }
    override fun onDestroy() {
        unregisterReceiver()
        super.onDestroy()
        mSocket.disconnect()
    }
    private fun loadNativeAd() {
        // Instantiate a NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = NativeAd(activity as Context, "353968571943372_356382235035339")
        nativeAd!!.setAdListener(object : NativeAdListener {
            override fun onMediaDownloaded(ad: Ad) {
                // Native ad finished downloading all assets
                Log.e(ContentValues.TAG, "Native ad finished downloading all assets.")
            }
            override fun onError(ad: Ad, adError: AdError) {
                // Native ad failed to load
                Log.e(ContentValues.TAG, "Native ad failed to load: " + adError.errorMessage)
            }
            override fun onAdLoaded(ad: Ad) {
                // Native ad is loaded and ready to be displayed
                Log.d(ContentValues.TAG, "Native ad is loaded and ready to be displayed!")
                if (nativeAd == null || nativeAd !== ad) {
                    return
                }
                // Inflate Native Ad into Container
                inflateAd(nativeAd!!)
            }
            override fun onAdClicked(ad: Ad) {
                // Native ad clicked
                Log.d(ContentValues.TAG, "Native ad clicked!")
            }
            override fun onLoggingImpression(ad: Ad) {
                // Native ad impression
                Log.d(ContentValues.TAG, "Native ad impression logged!")
            }
        })
        // Request an ad
        nativeAd!!.loadAd()
    }
    private fun inflateAd(nativeAd: NativeAd) {
        nativeAd.unregisterView()
        //wall_recycler = findViewById(R.id.wall_recycler_id) as RecyclerView
        // Add the Ad view into the ad container.
        nativeAdLayout = view!!.findViewById(R.id.view_pizza)
        val inflater = LayoutInflater.from(activity as Context)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView = inflater.inflate(R.layout.native_item, nativeAdLayout, false) as LinearLayout
        nativeAdLayout!!.addView(adView)
        // Add the AdOptionsView
        val adChoicesContainer = view!!.findViewById<LinearLayout>(R.id.ad_choices_container)
        val adOptionsView = AdOptionsView(activity as Context, nativeAd, nativeAdLayout)
        adChoicesContainer.removeAllViews()
        adChoicesContainer.addView(adOptionsView, 0)
        // Create native UI using the ad metadata.
        val nativeAdIcon = adView!!.findViewById<AdIconView>(R.id.native_ad_icon)
        val nativeAdTitle = adView!!.findViewById<TextView>(R.id.native_ad_title)
        val nativeAdMedia = adView!!.findViewById<MediaView>(R.id.native_ad_media)
        val nativeAdSocialContext = adView!!.findViewById<TextView>(R.id.native_ad_social_context)
        val nativeAdBody = adView!!.findViewById<TextView>(R.id.native_ad_body)
        val sponsoredLabel = adView!!.findViewById<TextView>(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction = adView!!.findViewById<Button>(R.id.native_ad_call_to_action)
        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.visibility = if (nativeAd.hasCallToAction()) VISIBLE else INVISIBLE
        nativeAdCallToAction.text = nativeAd.adCallToAction
        sponsoredLabel.text = nativeAd.sponsoredTranslation
        // Create a list of clickable views
        val clickableViews = ArrayList<View>()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
            adView,
            nativeAdMedia,
            nativeAdIcon,
            clickableViews
        )
       /* url=getArguments()!!.getString("url")
        Toast.makeText(context, " $`url`", Toast.LENGTH_SHORT).show()*/
        next = view!!.findViewById(R.id.btnNext)
        previous = view!!.findViewById(R.id.btnPrevious)
        //  title = view!!.findViewById(R.id.title)
        playme=view!!.findViewById(R.id.btnPlay)
        stopme=view!!.findViewById(R.id.btnStop)
        title=view!!.findViewById(R.id.tv_title)
        btnList=view!!.findViewById(R.id.btnList)

        btnSpeaker=view!!.findViewById(R.id.btnSpeaker)
        playme.setOnClickListener {
            // TODO Auto-generated method stub
            if (radioModelArrayList.size > 0) {
                val radioModel1 = radioModelArrayList[index_pos]
                Toast.makeText(context, " $pl", Toast.LENGTH_SHORT).show()
                val URL = radioModel1.link
                title.visibility=View.VISIBLE
                playRadio(URL)
                title.text = radioModel1.name
                Log.d("TAG", "link = " + URL!!)
            }
        }


        btnList.setOnClickListener {
            // TODO Auto-generated method stub
            val intent = Intent (this@DashboardFragment.context, RadioActivity::class.java)
            startActivity(intent)

        }
        btnSpeaker.setOnClickListener {
            // TODO Auto-generated method stub
            if(mAudioManager!!.isWiredHeadsetOn){
                mAudioManager = this@DashboardFragment.context!!.getSystemService(AUDIO_SERVICE) as AudioManager
                mAudioManager!!.setWiredHeadsetOn(false)
                mAudioManager!!.setSpeakerphoneOn(true)
                mAudioManager!!.setMode(AudioManager.MODE_IN_COMMUNICATION);

            }else{
                mAudioManager!!.setMode(AudioManager.MODE_IN_COMMUNICATION);
                mAudioManager!!.setSpeakerphoneOn(false)
                mAudioManager!!.setWiredHeadsetOn(true)
                Toast.makeText(this@DashboardFragment.context, "Wired Headset On", Toast.LENGTH_LONG).show();
            }


        }
        stopme.setOnClickListener {
            // TODO Auto-generated method stub
            stopService()
            Toast.makeText(context, "  $st", Toast.LENGTH_SHORT).show()
            playme.isEnabled = true
            stopme.isEnabled = false
        }

        next.setOnClickListener {
           stopService()
            index_pos++
            Log.d("TAG", "pos = $index_pos")
            if (radioModelArrayList.size > 0 && radioModelArrayList.size >= index_pos + 1) {
                val radioModel1 = radioModelArrayList[index_pos]
                Toast.makeText(context, " $pl", Toast.LENGTH_SHORT).show()
                val URL = radioModel1.link
                title.text = radioModel1.name
                playRadio(URL)
                Log.d("TAG", "link = " + URL!!)
            }
        }
        previous.setOnClickListener {
            mp.stop()
            mp.reset()
            index_pos--
            Log.d("TAG", "pos = $index_pos")
            if (radioModelArrayList.size > 0
                && radioModelArrayList.size >= index_pos + 1
                && index_pos >= 0) {
                val radioModel1 = radioModelArrayList[index_pos]
                Toast.makeText(context, " $pl", Toast.LENGTH_SHORT).show()
                val URL = radioModel1.link
                title.text = radioModel1.name
                playRadio(URL)
                Log.d("TAG", "link = " + URL!!)
            }
        }
    }
     fun stopService(){
         mp.stop()
         mp.reset()
    }
    private fun getRadio() {
        radioModelArrayList = ArrayList()
        var radioModel = RadioModel()
        radioModel.name = "Alex jones"
        radioModel.link = "http://192.240.107.134/stream"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "DnB Radio"
        radioModel.link = "http://source.dnbradio.com:10128/;"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "Kera 90.1 FM"
        radioModel.link = "http://kera-ice.streamguys.us:80/keralive"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KAJA KJ97"
        radioModel.link = "https://c7icy.prod.playlists.ihrhls.com/2337_icy"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KAYD FM 101.7 KD 101"
        radioModel.link = "http://17843.live.streamtheworld.com:80/KAYDFMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBAT FM 99.9"
        radioModel.link = "http://54.235.62.118/townsquare-kbatfmaac-ibc3"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBCY FM 99.7"
        radioModel.link = "http://17843.live.streamtheworld.com:80/KBCYFMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBCY FM 99.7"
        radioModel.link = "http://17843.live.streamtheworld.com:80/KBCYFMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBED AM 1510"
        radioModel.link = "http://14023.live.streamtheworld.com:80/KIKRAMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBEX-FM La Poderosa 96.1FM"
        radioModel.link = "http://net1.citrus3.com:9050/stream"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBNR Mananatial FM 98.3"
        radioModel.link = "http://ic2.christiannetcast.com/kbnr-fm"
        radioModelArrayList.add(radioModel) //To change body of created functions use File | Settings | File Templates.
    }
    private fun playRadio(radio_url: String?) {
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC)
        stopme.isEnabled = true
        playme.isEnabled = false
        // get data from internet ...
        try {
            mp.setDataSource(radio_url)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // buffer...
        try {
            mp.prepare()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mp.start()
    }
/*
    fun initializeView() {
        print("message")
        simpleDraweeView = view?.findViewById(R.id.view_pizza) as SimpleDraweeView
        AdSettings.addTestDevice("1d4fd4a5-5279-49fe-9e16-1679d9abe5ad")
        mAdView = AdView(activity as Context, "381851972454079_381853415787268", AdSize.BANNER_HEIGHT_50)
        mAdView!!.setAdListener(object : AdListener {
            override fun onError(ad: Ad, adError: AdError) {
                // Ad error callback
                Toast.makeText(
                    context, "Error: " + adError.errorMessage,
                    Toast.LENGTH_LONG
                ).show()
            }
            override fun onAdLoaded(ad: Ad) {
                // Ad loaded callback
            }
            override fun onAdClicked(ad: Ad) {
                // Ad clicked callback
            }
            override fun onLoggingImpression(ad: Ad) {
                // Ad impression logged callback
            }
        })
        mAdView!!.loadAd()
    }
*/
    /**
     * Ads Observer for observe the fetched ads
     */
    private val adsObserver = Observer<AdsResponse> { ads ->
        if (ads.playList.isNotEmpty()) {
            releasePlayer()
            adsList.clear()
            adsList.addAll(ads.playList)
            adsCounter = 0
            loadAds()
        }
    }
    private fun loadAds() {
        if (adsList[adsCounter].mimeType.contains("image", FLAG_TRUE)) {
            mViewDataBinding.videoAds.visibility = GONE
            mViewDataBinding.imageAds.visibility = VISIBLE
            val imageLoadListener = object : BaseControllerListener<ImageInfo>() {
                override fun onFinalImageSet(id: String?, @Nullable imageInfo: ImageInfo?, @Nullable animatable: Animatable?) {
                    Handler().postDelayed({
                        showNextAd()
                    }, IMAGE_AD_SHOW_TIME)
                }
                override fun onFailure(id: String?, throwable: Throwable?) {
                    showNextAd()
                }
            }
            val mImageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(adsList[adsCounter].url))
                .setLocalThumbnailPreviewsEnabled(true)
                .setCacheChoice(ImageRequest.CacheChoice.SMALL)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .setProgressiveRenderingEnabled(true)
                .build()
            val controller = Fresco.newDraweeControllerBuilder()
                .setLowResImageRequest(ImageRequest.fromUri(Uri.parse(adsList[adsCounter].url)))
                .setImageRequest(mImageRequest)
                .setControllerListener(imageLoadListener)
                .build()
            mViewDataBinding.imageAds.controller = controller
        } else if (adsList[adsCounter].mimeType.contains("video", FLAG_TRUE)) {
            mViewDataBinding.imageAds.visibility = GONE
            mViewDataBinding.videoAds.visibility = VISIBLE
            mVideoAdsPlayer = ExoPlayerFactory.newSimpleInstance(
                activity as Context,
                DefaultTrackSelector(),
                DefaultLoadControl()
            )
            mViewDataBinding.videoAds.player = mVideoAdsPlayer
            val uri = Uri.parse(adsList[adsCounter].url)
            val mediaSource = buildMediaSource(uri)
            mVideoAdsPlayer?.let { player ->
                player.playWhenReady = true
                player.addListener(object : Player.EventListener {
                    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
                    }
                    override fun onSeekProcessed() {
                    }
                    override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
                    }
                    override fun onPlayerError(error: ExoPlaybackException?) {
                        Log.e("PLAYER ERROR", "ERROR ${error!!.localizedMessage}")
                        showNextAd()
                    }
                    override fun onLoadingChanged(isLoading: Boolean) {
                    }
                    override fun onPositionDiscontinuity(reason: Int) {
                    }
                    override fun onRepeatModeChanged(repeatMode: Int) {
                    }
                    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
                    }
                    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
                    }
                    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                        when (playbackState) {
                            Player.STATE_IDLE -> {
                                Log.e("PLAYER", "IDLE")
                            }
                            Player.STATE_ENDED -> {
                                Log.e("PLAYER", "ENDS")
                                showNextAd()
                            }
                            Player.STATE_BUFFERING -> {
                                Log.e("PLAYER", "BUFFERING")
                            }
                            Player.STATE_READY -> {
                                Log.e("PLAYER", "PLAYING")
                            }
                            PlaybackState.STATE_ERROR -> {
                                Log.e("PLAYER", "ERROR ")
                                showNextAd()
                            }
                        }
                    }
                })
                player.prepare(mediaSource, true, false)
            }
        }
    }
    private fun showNextAd() {
        adsCounter++
        if (adsList.size == adsCounter) {
            adsCounter = 0
        }
        releasePlayer()
        loadAds()
    }
    private fun buildMediaSource(uri: Uri): MediaSource {
//        return ExtractorMediaSource
//            .Factory(DefaultHttpDataSourceFactory("exoplayer-codelab"))
//            .createMediaSource(uri)
        return ExtractorMediaSource(
            uri, videoCache,
            DefaultExtractorsFactory(), null, null
        )
    }
    private fun releasePlayer() {
        mVideoAdsPlayer?.let { player ->
            player.release()
            mVideoAdsPlayer = null
        }
    }
    override fun onStop() {
        releasePlayer()
        super.onStop()
    }

   /* override fun onResume() {
        super.onResume()
        url=getArguments()!!.getString("url")
        Toast.makeText(context, " $`url`", Toast.LENGTH_SHORT).show()

    }*/
    internal inner class CacheDataSourceFactory(
        private val context: Context,
        private val maxCacheSize: Long,
        private val maxFileSize: Long
    ) : DataSource.Factory {
        private val defaultDatasourceFactory: DefaultDataSourceFactory
        init {
            val userAgent = Util.getUserAgent(context, context.getString(R.string.app_name))
            val bandwidthMeter = DefaultBandwidthMeter()
            defaultDatasourceFactory = DefaultDataSourceFactory(
                this.context,
                bandwidthMeter,
                DefaultHttpDataSourceFactory(userAgent, bandwidthMeter)
            )
        }
        override fun createDataSource(): DataSource {
            val evictor = LeastRecentlyUsedCacheEvictor(maxCacheSize)
            //val simpleCache = SimpleCache(File(context.cacheDir, "media"), evictor)
            val simpleCache = VideoCache.getInstance(context)
            return CacheDataSource(
                simpleCache, defaultDatasourceFactory.createDataSource(),
                FileDataSource(), CacheDataSink(simpleCache, maxFileSize),
                CacheDataSource.FLAG_BLOCK_ON_CACHE or CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null
            )
        }
    }
    class VideoCache {
        companion object {
            lateinit var mDownloadCache: SimpleCache
            fun getInstance(context: Context): SimpleCache {
                if (!::mDownloadCache.isInitialized) {
                    mDownloadCache =
                        SimpleCache(
                            File(context.cacheDir, "media"),
                            LeastRecentlyUsedCacheEvictor(100 * 1024 * 1024)
                        )
                } else {
                    if (mDownloadCache == null) {
                        mDownloadCache =
                            SimpleCache(
                                File(context.cacheDir, "media"),
                                LeastRecentlyUsedCacheEvictor(100 * 1024 * 1024)
                            )
                    }
                }
                return mDownloadCache
            }
        }
    }
    private fun registerReceiver() {
        LocalBroadcastManager.getInstance(activity as Context)
            .registerReceiver(updatePlaylistBroadcastReceiver, intentFilter)
    }
    private fun unregisterReceiver() {
        LocalBroadcastManager.getInstance(activity as Context).unregisterReceiver(updatePlaylistBroadcastReceiver)
    }
    private inner class UpdatePlaylistBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                val age = it.getIntExtra(NEW_AGE, -1)
                val gender = it.getStringExtra(NEW_GENDER).toUpperCase()
                mViewModel.fetchAds(age, if (gender.isEmpty()) MALE else gender)
            }
        }
    }
}