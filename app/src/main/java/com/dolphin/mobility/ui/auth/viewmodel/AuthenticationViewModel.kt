package com.dolphin.mobility.ui.auth.viewmodel

import android.app.Application
import androidx.databinding.Bindable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dolphin.mobility.BR
import com.dolphin.mobility.base.utils.Constants.FLAG_TRUE
import com.dolphin.mobility.base.utils.Constants.IS_LOGGED_IN
import com.dolphin.mobility.base.utils.Constants.OPEN_DASHBOARD_ACTICITY
import com.dolphin.mobility.base.utils.Constants.USER_NAME
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.base.viewmodel.BaseViewModel
import com.dolphin.mobility.data.request.VerifyOtpRequest
import com.dolphin.mobility.data.response.RequestOtpResponse
import com.dolphin.mobility.data.response.VerifyOtpResponse

/**
 * Created by Dhaval Parmar on 22/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class AuthenticationViewModel(application: Application) : BaseViewModel(application) {

    @get:Bindable
    var email: String = ""
        set(email) {
            field = email
            notifyPropertyChanged(BR.email)
        }

    @get:Bindable
    var otp: String = ""
        set(otp) {
            field = otp
            notifyPropertyChanged(BR.otp)
        }
    private val requestOtpLiveData = MutableLiveData<RequestOtpResponse>()
    /**
     * Method to get the OTP for requested user to authenticate the use
     */
    fun getOtp(email: String) {
        if (email.trim().isEmpty()) {
            setErrorMessage("Enter Email or Phone number.")
            return
        }

        mDisposable = mNetworkClient.requestOtp(email, this@AuthenticationViewModel)!!
    }

    fun verifyOtp(otp: String) {
        if (otp.trim().isEmpty()) {
            setErrorMessage("Enter OTP to verify.")
            return
        }

        mDisposable =
            mNetworkClient.verifyOtp(VerifyOtpRequest(otp = otp, userId = email), this@AuthenticationViewModel)!!
    }

    /**
     * Method to handle received OTP and navigate to screen accordingly
     */
    fun onReceiveOtp(it: RequestOtpResponse?) {
        requestOtpLiveData.postValue(it)
    }

    /**
     * Method to handle and navigation according to verification of user
     */
    fun onVerifyOtp(verifyOtp: VerifyOtpResponse?) {
        PreferenceManager.getInstance().apply {
            save(IS_LOGGED_IN, FLAG_TRUE)
            save(USER_NAME, verifyOtp?.name)
//            verifyOtp?.rideInfo?.vehicleNumber?.let {
//                save(VEHICLE_NUMBER, it)
//            }
        }
        pageNavigation.postValue(OPEN_DASHBOARD_ACTICITY)
    }

    /**
     * Observer of requesting OTP
     */
    fun getRequestedOtp(): LiveData<RequestOtpResponse> {
        return requestOtpLiveData
    }
}