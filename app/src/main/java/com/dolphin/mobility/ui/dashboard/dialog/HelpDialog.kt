package com.dolphin.mobility.ui.dashboard.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseDialogFragment
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.data.response.HelpData
import com.dolphin.mobility.databinding.DialogHelpBinding
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import kotlinx.android.synthetic.main.dialog_help.*
import kotlinx.android.synthetic.main.item_help_text.view.*


/**
 * Created by Dhaval Parmar on 23/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class HelpDialog : BaseDialogFragment<DialogHelpBinding, DashboardViewModel>() {

    override fun getLayout(): Int = R.layout.dialog_help

    override fun getBindingVariable(): Int = BR.helpViewModel

    override fun getViewModel(): DashboardViewModel = ViewModelProviders
        .of(this@HelpDialog)
        .get(DashboardViewModel::class.java)

    override fun initData() {
        mViewModel.fetchHelpDetails()
        (recycler_help.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

    override fun initLiveDataObservables() {
        mViewModel.getHelpData().observe(this, helpDataObserver)
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout((width * 0.6).toInt(), (height * 0.6).toInt())
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    activity as Context,
                    android.R.color.transparent
                )
            )
        )
    }

    private val helpDataObserver = Observer<List<HelpData>> {
        recycler_help.adapter = HelpDataAdapter(it)
    }

    class HelpDataAdapter(private val mData: List<HelpData>) :
        RecyclerView.Adapter<HelpDataAdapter.HelpDataViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HelpDataViewHolder {
            return HelpDataViewHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_help_text, parent, FLAG_FALSE)
            )
        }

        override fun getItemCount(): Int = mData.size

        override fun onBindViewHolder(holder: HelpDataViewHolder, position: Int) {

            holder.itemView.item_linear_help_data.visibility =
                if (mData[position].isExpanded) View.VISIBLE else View.GONE
            holder.itemView.item_help_text_title.text = mData[position].question
            holder.itemView.item_help_text_data.text = mData[position].answer

            holder.itemView.setOnClickListener {
                val isExpanded = mData[position].isExpanded
                mData[position].isExpanded = (!isExpanded)
                notifyItemChanged(position)
            }
        }

        inner class HelpDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    }

}