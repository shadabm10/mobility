package com.dolphin.mobility.ui.auth.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseDialogFragment
import com.dolphin.mobility.base.utils.ActivityManager
import com.dolphin.mobility.base.utils.Constants.OPEN_DASHBOARD_ACTICITY
import com.dolphin.mobility.base.utils.Constants.REQUESTED_OTP_DATA
import com.dolphin.mobility.data.request.VerifyOtpRequest
import com.dolphin.mobility.databinding.DialogVerifyOtpBinding
import com.dolphin.mobility.ui.auth.activity.AuthenticationActivity
import com.dolphin.mobility.ui.auth.viewmodel.AuthenticationViewModel
import com.dolphin.mobility.ui.dashboard.activity.DashboardActivity

/**
 * Created by Dhaval Parmar on 23/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class VerifyOtpDialog : BaseDialogFragment<DialogVerifyOtpBinding, AuthenticationViewModel>() {

    override fun getLayout(): Int = R.layout.dialog_verify_otp

    override fun getBindingVariable(): Int = BR.authViewModel

    override fun getViewModel(): AuthenticationViewModel = ViewModelProviders
        .of(this@VerifyOtpDialog)
        .get(AuthenticationViewModel::class.java)

    override fun initData() {
        arguments?.let { receivedData ->
            val data = receivedData.getParcelable<VerifyOtpRequest>(REQUESTED_OTP_DATA)!!
            mViewModel.email = data.userId
            mViewModel.otp = data.otp
        }
    }

    override fun initLiveDataObservables() {
        mViewModel.getPageRedirection().observe(this, navigateObserver)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        dialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    activity as Context,
                    R.color.transparentBlack
                )
            )
        )
    }

    /**
     * To navigate to particular activity
     */
    private val navigateObserver = Observer<Int> { t ->
        when (t) {
            OPEN_DASHBOARD_ACTICITY ->
                ActivityManager.navigateToFreshActivityWithAnimation(
                    activity as AuthenticationActivity,
                    DashboardActivity::class.java,
                    R.anim.anim_enter_right,
                    R.anim.anim_exit_right
                )
        }
    }

}