package com.dolphin.mobility.ui.dashboard.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.media.AudioManager
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Gravity
import android.view.WindowManager
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseDialogFragment
import com.dolphin.mobility.base.utils.ActivityManager
import com.dolphin.mobility.base.utils.Constants.OPEN_DASHBOARD_ACTICITY
import com.dolphin.mobility.databinding.DialogVolumeBinding
import com.dolphin.mobility.ui.auth.activity.AuthenticationActivity
import com.dolphin.mobility.ui.dashboard.activity.DashboardActivity
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import kotlinx.android.synthetic.main.dialog_volume.*


/**
 * Created by Dhaval Parmar on 23/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class VolumeDialog : BaseDialogFragment<DialogVolumeBinding, DashboardViewModel>() {

    override fun getLayout(): Int = R.layout.dialog_volume

    override fun getBindingVariable(): Int = BR.volumeViewModel

    override fun getViewModel(): DashboardViewModel = ViewModelProviders
        .of(this@VolumeDialog)
        .get(DashboardViewModel::class.java)

    override fun initData() {
        val audioManager = activity?.getSystemService(Context.AUDIO_SERVICE) as AudioManager?

        audioManager?.let {
            seek_volume.progress = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
            seek_volume.max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)

            seek_volume.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, newVolume: Int, b: Boolean) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, newVolume, 0)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {}

                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })

            image_volume_mute.setOnClickListener {
                seek_volume.progress = 0
            }
        }
    }

    override fun initLiveDataObservables() {
        mViewModel.getPageRedirection().observe(this, navigateObserver)
    }

    override fun onStart() {
        super.onStart()

        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        var actionBarHeight = 175
        val tv = TypedValue()
        if (activity?.theme?.resolveAttribute(android.R.attr.actionBarSize, tv, true)!!) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
        }

        dialog?.window?.setGravity(Gravity.BOTTOM)
        dialog?.window?.setLayout((width * 0.07).toInt(), (height * 0.55).toInt())
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setBackgroundDrawable(
            InsetDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        activity as Context,
                        android.R.color.transparent
                    )
                ), 0, 0, 0, actionBarHeight
            )
        )
    }

    /**
     * To navigate to particular activity
     */
    private val navigateObserver = Observer<Int> { t ->
        when (t) {
            OPEN_DASHBOARD_ACTICITY -> {
                ActivityManager.navigateToFreshActivityWithAnimation(
                    activity as AuthenticationActivity,
                    DashboardActivity::class.java,
                    R.anim.anim_enter_right,
                    R.anim.anim_exit_right
                )
            }
        }
    }

}