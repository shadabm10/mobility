package com.dolphin.mobility.ui.splash.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.base.utils.Constants.IS_LOGGED_IN
import com.dolphin.mobility.base.utils.Constants.OPEN_AUTHENTICATION_ACTIVITY
import com.dolphin.mobility.base.utils.Constants.OPEN_DASHBOARD_ACTICITY
import com.dolphin.mobility.base.utils.PreferenceManager
import com.dolphin.mobility.base.viewmodel.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by Dhaval Parmar on 22/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class SplashViewModel(application: Application) : BaseViewModel(application) {

    private lateinit var navigateDisposable: Disposable
    private val redirect = MutableLiveData<Int>()

    /**
     * To navigate next screen
     */
    fun startSeeding() {
        navigateDisposable = Observable.interval(3, 2, TimeUnit.SECONDS)
            .timeInterval()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                navigateToNextScreen()
            }
    }

    /**
     * To decide where to go next
     */
    private fun navigateToNextScreen() {
        navigateDisposable.dispose()
        if (PreferenceManager.getInstance()[IS_LOGGED_IN, FLAG_FALSE]) {
            redirect.postValue(OPEN_DASHBOARD_ACTICITY)
        } else {
            redirect.postValue(OPEN_AUTHENTICATION_ACTIVITY)
        }
    }

    /**
     * For callback to activity
     */
    fun getRedirection(): LiveData<Int> {
        return redirect
    }
}