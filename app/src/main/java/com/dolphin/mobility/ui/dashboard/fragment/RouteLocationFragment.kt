package com.dolphin.mobility.ui.dashboard.fragment

import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.DolphinMobilityApp
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseFragment
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.base.utils.PermissionManager
import com.dolphin.mobility.base.utils.callback.OnSearchLocationItemClickCallback
import com.dolphin.mobility.data.response.*
import com.dolphin.mobility.databinding.FragmentRouteLocationBinding
import com.dolphin.mobility.ui.dashboard.activity.DashboardActivity
import com.dolphin.mobility.ui.dashboard.adapter.SearchLocationAdapter
import com.dolphin.mobility.ui.dashboard.viewmodel.LocationViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_route_location.*
import java.util.concurrent.TimeUnit

/**
 * Created by Dhaval Parmar on 2019-05-21.
 * Email : dhvlparmar7@gmail.com
 */
class RouteLocationFragment : BaseFragment<FragmentRouteLocationBinding, LocationViewModel>(), OnMapReadyCallback,
    OnSearchLocationItemClickCallback {

    private lateinit var mapView: View
    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private var mSearchLocationList = arrayListOf<Prediction>()
    private lateinit var mSearchLocationAdapter: SearchLocationAdapter

    override fun getLayout(): Int = R.layout.fragment_route_location

    override fun getBindingVariable(): Int = BR.locationViewModel

    override fun getViewModel(): LocationViewModel = ViewModelProviders
        .of(this@RouteLocationFragment)
        .get(LocationViewModel::class.java)

    override fun initData() {
        mapFragment = childFragmentManager
            .findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapView = mapFragment.view!!

        mSearchLocationAdapter =
            SearchLocationAdapter(activity as Context, R.layout.item_search_location, mSearchLocationList, this)
        autocomplete_search_location.setAdapter(mSearchLocationAdapter)

        RxSearchObservable.fromView(autocomplete_search_location)
            .debounce(1500, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { searchQuery ->
                if (searchQuery.length > 0)
                    mViewModel.getLocations(searchQuery)
            }
    }

    override fun initLiveDataObservables() {
        mViewModel.observSearchLocation().observe(this@RouteLocationFragment, searchLocationObserver)
        mViewModel.observePlaceDetails().observe(this@RouteLocationFragment, placeDetailsObserver)
        mViewModel.getDirectionPath().observe(this@RouteLocationFragment, pathObserver)
    }

    /**
     * Observer for search location.
     * */
    private val searchLocationObserver = Observer<SearchLocation> {
        mSearchLocationList.clear()
        mSearchLocationList.addAll(it?.predictions!!)
        mSearchLocationAdapter.notifyDataSetChanged()
    }

    /**
     * Place Details Observer
     */
    private val placeDetailsObserver = Observer<PlaceDetailResponse> {
        if (it?.status == "OK") {
            val queriedLocation = it.result.geometry.location
            queriedLocation.let {
                autocomplete_search_location.dismissDropDown()
                val origin =
                    "${DolphinMobilityApp.currentLocation?.latitude},${DolphinMobilityApp.currentLocation?.longitude}"
                val destination = "${queriedLocation.lat},${queriedLocation.lng}"
                mViewModel.loadPath(origin, destination)
            }
        } else {
            Toast.makeText(activity, "Please try again later", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Direction Data Observer
     */
    private val pathObserver = Observer<DirectionData> {
        if (it?.success == "OK") {
            val pathList: MutableList<Routes> = mutableListOf()
            val points: MutableList<String> = mutableListOf()

            pathList.addAll(it!!.routes)
            for (point in pathList) {
                points.add(point.overviewPolyLine.points)
            }

            var route = listOf<LatLng>()
            for (point in points) {
                route = PolyUtil.decode(point)
            }

            mMap.clear()

            mMap.addPolyline(
                PolylineOptions()
                    .addAll(route)
                    .width(15.0f)
                    .color(Color.BLUE)
                    .geodesic(true)
            )

            if (route.isNotEmpty()) {
                val builder = LatLngBounds.Builder()
                route.forEach { bound ->
                    builder.include(bound)
                }
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngBounds(
                        builder.build(), 100
                    )
                )
            }
        } else {
            mMap.clear()
        }
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        if (PermissionManager.hasLocationPermission()) {
            mMap.isMyLocationEnabled = true
        } else {
            (activity as DashboardActivity).checkLocationIsEnableOrNot()
        }
        mMap.uiSettings.isRotateGesturesEnabled = false
        mMap.uiSettings.isTiltGesturesEnabled = false
        mMap.setMaxZoomPreference(14f)
        DolphinMobilityApp.currentLocation?.let {
            val mLatLongSource =
                LatLng(it.latitude, it.longitude)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLongSource, 14f))
        }
    }

    override fun onSearchLocation(placeId: String, placeName: String) {
        mViewModel.getPlaceById(placeId)
        autocomplete_search_location.setText(placeName, FLAG_FALSE)
    }

    object RxSearchObservable {

        fun fromView(searchView: AutoCompleteTextView): Observable<String> {

            val subject: PublishSubject<String> = PublishSubject.create<String>()

            searchView.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    subject.onNext(s.toString())
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

            })
            return subject
        }
    }
}