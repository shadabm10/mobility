package com.dolphin.mobility.ui.dashboard.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.dolphin.mobility.base.utils.RadioUtil
import android.os.Bundle
import com.dolphin.mobility.R
import com.dolphin.mobility.ui.dashboard.activity.DashboardActivity
import com.dolphin.mobility.ui.dashboard.activity.RadioActivity
import com.dolphin.mobility.ui.dashboard.fragment.DashboardFragment






class CustomRecyclerAdapter(private val context: Context, personUtils: List<*>) : RecyclerView.Adapter<CustomRecyclerAdapter.ViewHolder>() {
    private val personUtils: List<RadioUtil>

    init {
        this.personUtils = personUtils as List<RadioUtil>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.single_list_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tag = personUtils[position]

        val pu = personUtils[position]

        holder.pName.text = pu.channelName
        holder.itemView.setOnClickListener {

        }

    }

    override fun getItemCount(): Int {
        return personUtils.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var pName: TextView

        init {

            pName = itemView.findViewById<View>(R.id.pNametxt) as TextView

            itemView.setOnClickListener { view ->
                val cpu = view.tag as RadioUtil
                val intent = Intent (view.context, DashboardActivity::class.java)
                view.context.startActivity(intent)
               // Toast.makeText(view.context, cpu.channelName + " is " + cpu.url, Toast.LENGTH_SHORT).show()
            }

        }
    }

}