package com.dolphin.mobility.ui.auth.fragment

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseFragment
import com.dolphin.mobility.base.utils.Constants.REQUESTED_OTP_DATA
import com.dolphin.mobility.base.utils.Constants.SHOW_OTP_DIALOG
import com.dolphin.mobility.data.request.VerifyOtpRequest
import com.dolphin.mobility.data.response.RequestOtpResponse
import com.dolphin.mobility.databinding.FragmentLoginBinding
import com.dolphin.mobility.ui.auth.dialog.VerifyOtpDialog
import com.dolphin.mobility.ui.auth.viewmodel.AuthenticationViewModel

/**
 * Created by Dhaval Parmar on 22/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class LoginFragment : BaseFragment<FragmentLoginBinding, AuthenticationViewModel>() {

    private lateinit var requestedOtpData: VerifyOtpRequest

    override fun getLayout(): Int = R.layout.fragment_login

    override fun getBindingVariable(): Int = BR.authViewModel

    override fun getViewModel(): AuthenticationViewModel = ViewModelProviders
        .of(this@LoginFragment)
        .get(AuthenticationViewModel::class.java)

    override fun initData() {
    }

    override fun initLiveDataObservables() {
        mViewModel.getPageRedirection().observe(this@LoginFragment, navigateObserver)
        mViewModel.getRequestedOtp().observe(this@LoginFragment, requestedOtpObserver)
    }

    /**
     * To handle requested OTP
     */
    private val requestedOtpObserver = Observer<RequestOtpResponse> { result ->
        if (result.otpSent) {
            requestedOtpData = VerifyOtpRequest(otp = result.otp.toString(), userId = result.userId)
            navigateObserver.onChanged(SHOW_OTP_DIALOG)
        } else {
            Toast.makeText(activity as Context, "Unable to send OTP", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * To navigate to particular activity
     */
    private val navigateObserver = Observer<Int> { t ->
        when (t) {
            SHOW_OTP_DIALOG -> {
                val dialog = VerifyOtpDialog()
                val mBundle = Bundle()
                mBundle.putParcelable(REQUESTED_OTP_DATA, requestedOtpData)
                dialog.arguments = mBundle
                dialog.show(activity!!.supportFragmentManager, VerifyOtpDialog::class.java.name)
            }
//            OPEN_DASHBOARD_ACTICITY -> {
//                ActivityManager.navigateToFreshActivityWithAnimation(
//                    activity as AuthenticationActivity,
//                    DashboardActivity::class.java,
//                    R.anim.anim_enter_right,
//                    R.anim.anim_exit_right
//                )
//            }
        }
    }

}