package com.dolphin.mobility.ui.dashboard.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseDialogFragment
import com.dolphin.mobility.data.response.SupportData
import com.dolphin.mobility.databinding.DialogSupportBinding
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import kotlinx.android.synthetic.main.dialog_support.*

/**
 * Created by Dhaval Parmar on 23/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class SupportDialog : BaseDialogFragment<DialogSupportBinding, DashboardViewModel>() {

    override fun getLayout(): Int = R.layout.dialog_support

    override fun getBindingVariable(): Int = BR.supportViewModel

    override fun getViewModel(): DashboardViewModel = ViewModelProviders
        .of(this@SupportDialog)
        .get(DashboardViewModel::class.java)

    override fun initData() {
        mViewModel.fetchSupportDetails()
    }

    override fun initLiveDataObservables() {
        mViewModel.getSupportData().observe(this@SupportDialog, supportDataObserver)
    }

    private val supportDataObserver = Observer<List<SupportData>> {
        it?.let { supportData ->
            supportData[0].let { data ->
                text_support_number.text = data.helplineNumber
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels

        dialog?.window?.setLayout((width * 0.6).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    activity as Context,
                    android.R.color.transparent
                )
            )
        )
    }
}