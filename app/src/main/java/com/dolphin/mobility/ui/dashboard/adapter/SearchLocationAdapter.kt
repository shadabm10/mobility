package com.dolphin.mobility.ui.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.annotation.NonNull
import com.dolphin.mobility.base.utils.Constants.FLAG_FALSE
import com.dolphin.mobility.base.utils.callback.OnSearchLocationItemClickCallback
import com.dolphin.mobility.data.response.Prediction
import kotlinx.android.synthetic.main.item_search_location.view.*

/**
 * Created by Dhaval Parmar on 2019-05-22.
 * Email : dhvlparmar7@gmail.com
 */
class SearchLocationAdapter(
    mContext: Context,
    private val itemLayout: Int,
    private val mData: List<Prediction>,
    private val listener: OnSearchLocationItemClickCallback
) :
    ArrayAdapter<Prediction>(mContext, itemLayout, mData) {

    override fun getCount(): Int = mData.size

    override fun getItem(position: Int): Prediction? = mData[position]

    override fun getView(position: Int, convertView: View?, @NonNull parent: ViewGroup): View {
        val itemView = LayoutInflater.from(parent.context).inflate(itemLayout, parent, FLAG_FALSE)
        itemView.text_item_search_place_name.text = mData[position].structuredFormatting.mainText
        itemView.text_location_description.text = mData[position].structuredFormatting.secondaryText

        itemView.setOnClickListener {
            listener.onSearchLocation(
                mData[position].placeId,
                mData[position].structuredFormatting.mainText
            )
        }
        return itemView
    }
}