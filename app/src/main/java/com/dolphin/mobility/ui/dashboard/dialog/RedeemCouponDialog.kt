package com.dolphin.mobility.ui.dashboard.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseDialogFragment
import com.dolphin.mobility.base.utils.Constants.COUPON_CODE
import com.dolphin.mobility.base.utils.Constants.COUPON_DETAILS
import com.dolphin.mobility.data.response.BaseResponse
import com.dolphin.mobility.databinding.DialogRedeemCouponBinding
import com.dolphin.mobility.ui.dashboard.viewmodel.DashboardViewModel
import kotlinx.android.synthetic.main.dialog_redeem_coupon.*

/**
 * Created by Dhaval Parmar on 23/04/19.
 * Email : dhvlparmar7@gmail.com
 */
class RedeemCouponDialog : BaseDialogFragment<DialogRedeemCouponBinding, DashboardViewModel>() {

    override fun getLayout(): Int = R.layout.dialog_redeem_coupon

    override fun getBindingVariable(): Int = BR.redeemCouponViewModel

    override fun getViewModel(): DashboardViewModel = ViewModelProviders
        .of(this@RedeemCouponDialog)
        .get(DashboardViewModel::class.java)

    override fun initData() {
        arguments?.let {
            text_coupon_title.text = it.getString(COUPON_DETAILS)
            mViewModel.couponCode = it.getString(COUPON_CODE, "")
        }
    }

    override fun initLiveDataObservables() {
        mViewModel.getredeemCouponData().observe(this@RedeemCouponDialog, redeemCouponObserver)
    }

    override fun onStart() {
        super.onStart()
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        dialog?.window?.setLayout((width * 0.75).toInt(), (height * 0.6).toInt())
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    activity as Context,
                    android.R.color.transparent
                )
            )
        )
    }

    private val redeemCouponObserver = Observer<BaseResponse> {
        if (it.statusCode == 200) {
            Toast.makeText(activity as Context, "Coupon sent to your email address", Toast.LENGTH_LONG).show()
            dismiss()
        }
    }
}