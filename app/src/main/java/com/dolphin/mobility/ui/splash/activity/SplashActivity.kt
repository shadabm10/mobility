package com.dolphin.mobility.ui.splash.activity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolphin.mobility.BR
import com.dolphin.mobility.R
import com.dolphin.mobility.base.ui.BaseActivity
import com.dolphin.mobility.base.utils.ActivityManager
import com.dolphin.mobility.base.utils.Constants.OPEN_AUTHENTICATION_ACTIVITY
import com.dolphin.mobility.base.utils.Constants.OPEN_DASHBOARD_ACTICITY
import com.dolphin.mobility.databinding.ActivitySplashBinding
import com.dolphin.mobility.ui.auth.activity.AuthenticationActivity
import com.dolphin.mobility.ui.dashboard.activity.DashboardActivity
import com.dolphin.mobility.ui.splash.viewmodel.SplashViewModel


class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {


    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun getViewModel(): SplashViewModel = ViewModelProviders
        .of(this@SplashActivity)
        .get(SplashViewModel::class.java)

    override fun getBindingVariable(): Int = BR._all

    override fun initLiveDataObservables() {
        mViewModel.getRedirection().observe(this, navigateObserver)
        mViewModel.startSeeding()
    }

    override fun initData() {

    }

    /**
     * To navigate to particular activity
     */
    private val navigateObserver = Observer<Int> { t ->
        when (t) {
            OPEN_AUTHENTICATION_ACTIVITY -> ActivityManager.startFreshActivity(
                this@SplashActivity,
                AuthenticationActivity::class.java
            )

            OPEN_DASHBOARD_ACTICITY -> ActivityManager.navigateToFreshActivityWithAnimation(
                this@SplashActivity,
                DashboardActivity::class.java,
                R.anim.anim_enter_right,
                R.anim.anim_exit_right
            )
        }
    }
}
