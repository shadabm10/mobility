package com.dolphin.mobility.ui.dashboard.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dolphin.mobility.base.viewmodel.BaseViewModel
import com.dolphin.mobility.data.response.DirectionData
import com.dolphin.mobility.data.response.PlaceDetailResponse
import com.dolphin.mobility.data.response.SearchLocation

/**
 * Created by Dhaval Parmar on 2019-05-21.
 * Email : dhvlparmar7@gmail.com
 */
class LocationViewModel(application: Application) : BaseViewModel(application) {

    private var searchLocation = MutableLiveData<SearchLocation>()
    private var placeDetails = MutableLiveData<PlaceDetailResponse>()
    private val pathData = MutableLiveData<DirectionData>()

    fun getLocations(query: String?) {
        mDisposable = mNetworkClient.searchPlace(query, this)!!
    }

    /**
     * Observer function for search location to find route.
     * */
    fun observSearchLocation(): MutableLiveData<SearchLocation> {
        return searchLocation
    }

    /**
     * Observer fo success of getNearbyLocations Api.
     * */
    fun onSuccessGetLocation(it: SearchLocation?) {
        searchLocation.postValue(it)
    }

    /**
     * Getting the place buy it's ID
     */
    fun getPlaceById(placeId: String) {
        mDisposable = mNetworkClient.getPlaceDetailsById(placeId, this@LocationViewModel)!!
    }

    /**
     * Observer function for Place Details of Location.
     * */
    fun observePlaceDetails(): MutableLiveData<PlaceDetailResponse> {
        return placeDetails
    }

    /**
     * On Success of Getting Place Details
     */
    fun onSuccessFetchPlaceDetails(it: PlaceDetailResponse) {
        placeDetails.postValue(it)
    }

    /**
     * Load path between two locations
     */
    fun loadPath(origin: String, destination: String) {
        mDisposable = mNetworkClient.fetchPath(origin, destination, this@LocationViewModel)!!
    }

    /**
     *
     */
    fun getDirectionPath(): LiveData<DirectionData> {
        return pathData
    }

    /**
     * On fetch direction between two locations
     */
    fun onFetchPath(result: DirectionData?) {
        pathData.postValue(result)
    }
}