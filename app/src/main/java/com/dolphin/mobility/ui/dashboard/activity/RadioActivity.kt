package com.dolphin.mobility.ui.dashboard.activity

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dolphin.mobility.R
import com.dolphin.mobility.base.utils.RadioUtil
import com.dolphin.mobility.ui.auth.viewmodel.RadioModel
import com.dolphin.mobility.ui.dashboard.adapter.CustomRecyclerAdapter
import java.util.ArrayList
import java.util.logging.Logger
import android.os.Build



class RadioActivity : AppCompatActivity() {

    internal lateinit var recyclerView: RecyclerView
    internal lateinit var imgback: ImageView
    internal lateinit var mAdapter: RecyclerView.Adapter<*>
    internal lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var radioModelArrayList: ArrayList<RadioModel>
    private var mAudioManager: AudioManager? = null
    private lateinit var playme: ImageButton
    private lateinit var stopme: ImageButton
    private lateinit var next: ImageButton
    private lateinit var previous: ImageButton
    private lateinit var title: TextView
    private lateinit var btnList: ImageButton
    private lateinit var btnSpeaker: ImageButton
    private lateinit var mute: ImageView
    private lateinit var imgmuteunmute: ImageView
    private var index_pos = 0
    private var VolIsMute: Boolean = false

    var str: String  = "http://192.240.107.134/stream"

    private var mp = MediaPlayer()
    internal lateinit var personUtilsList: MutableList<RadioUtil>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio_station_list)
        Logger.getLogger(RadioActivity::class.java.name).warning(str)


        recyclerView = findViewById<View>(R.id.recyclerView) as RecyclerView
        imgback = findViewById<View>(R.id.img_back) as ImageView
        recyclerView.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(this)

        recyclerView.layoutManager = layoutManager

        personUtilsList = ArrayList()
        getRadio()
        //Adding Data into ArrayList
        personUtilsList.add(RadioUtil("Alex jones",  "http://192.240.107.134/stream"))
        personUtilsList.add(RadioUtil("DnB Radio",  "http://source.dnbradio.com:10128/;"))
        personUtilsList.add(RadioUtil("Kera 90.1 FM", "http://kera-ice.streamguys.us:80/keralive"))
        personUtilsList.add(RadioUtil("KAJA KJ97",  "https://c7icy.prod.playlists.ihrhls.com/2337_icy"))
        personUtilsList.add(RadioUtil("KAYD FM 101.7 KD 101",  "http://17843.live.streamtheworld.com:80/KAYDFMAAC_SC"))
        personUtilsList.add(RadioUtil("KBAT FM 99.9",  "http://54.235.62.118/townsquare-kbatfmaac-ibc3"))
        personUtilsList.add(RadioUtil("KBCY FM 99.7",  "http://17843.live.streamtheworld.com:80/KBCYFMAAC_SC"))
        personUtilsList.add(RadioUtil("KBCY FM 99.7",  "http://17843.live.streamtheworld.com:80/KBCYFMAAC_SC"))
        personUtilsList.add(RadioUtil("KBED AM 1510",  "http://14023.live.streamtheworld.com:80/KIKRAMAAC_SC"))
        personUtilsList.add(RadioUtil("KBEX-FM La Poderosa 96.1FM", "http://net1.citrus3.com:9050/stream"))
        personUtilsList.add(RadioUtil("KBNR Mananatial FM 98.3", "http://ic2.christiannetcast.com/kbnr-fm"))

        mAdapter = CustomRecyclerAdapter(this, personUtilsList)

        recyclerView.adapter = mAdapter
        next = findViewById(R.id.btnNext)
        previous = findViewById(R.id.btnPrevious)
        //  title = view!!.findViewById(R.id.title)
        playme=findViewById(R.id.btnPlay)
        stopme=findViewById(R.id.btnStop)
        title=findViewById(R.id.tv_title)
        mute=findViewById(R.id.img_mute)
        imgmuteunmute=findViewById(R.id.img_mute_unmute)
      //  btnList=findViewById(R.id.btnList)
        btnSpeaker=findViewById(R.id.btnSpeaker)
        playme.setOnClickListener {
            // TODO Auto-generated method stub
            if (radioModelArrayList.size > 0) {
                val radioModel1 = radioModelArrayList[index_pos]
              //  Toast.makeText(context, " $pl", Toast.LENGTH_SHORT).show()
                val URL = radioModel1.link
                title.visibility=View.VISIBLE
                playRadio(URL)
                title.text = radioModel1.name
                Log.d("TAG", "link = " + URL!!)
            }
        }
        btnSpeaker.setOnClickListener {
            // TODO Auto-generated method stub
            if(mAudioManager!!.isWiredHeadsetOn){
                mAudioManager = this@RadioActivity.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                mAudioManager!!.setWiredHeadsetOn(false)
                mAudioManager!!.setSpeakerphoneOn(true)
                mAudioManager!!.setMode(AudioManager.MODE_IN_COMMUNICATION);

            }else{
                mAudioManager!!.setMode(AudioManager.MODE_IN_COMMUNICATION);
                mAudioManager!!.setSpeakerphoneOn(false)
                mAudioManager!!.setWiredHeadsetOn(true)
                Toast.makeText(this@RadioActivity, "Wired Headset On", Toast.LENGTH_LONG).show();
            }


        }
        mute.setOnClickListener {

          imgmuteunmute.visibility=View.VISIBLE
            mute.visibility=View.GONE

           MuteAudio()
        }
        imgmuteunmute.setOnClickListener {
            imgmuteunmute.visibility=View.GONE
            mute.visibility=View.VISIBLE

           UnMuteAudio()
        }
        stopme.setOnClickListener {
            // TODO Auto-generated method stub
            mp.stop()
            mp.reset()
           // Toast.makeText(context, "  $st", Toast.LENGTH_SHORT).show()
            playme.isEnabled = true
            stopme.isEnabled = false
        }

        next.setOnClickListener {
            mp.stop()
            mp.reset()
            index_pos++
            Log.d("TAG", "pos = $index_pos")
            if (radioModelArrayList.size > 0 && radioModelArrayList.size >= index_pos + 1) {
                val radioModel1 = radioModelArrayList[index_pos]
               // Toast.makeText(context, " $pl", Toast.LENGTH_SHORT).show()
                val URL = radioModel1.link
                title.text = radioModel1.name
                playRadio(URL)
                Log.d("TAG", "link = " + URL!!)
            }
        }
        previous.setOnClickListener {
           stopService()
            index_pos--
            Log.d("TAG", "pos = $index_pos")
            if (radioModelArrayList.size > 0
                && radioModelArrayList.size >= index_pos + 1
                && index_pos >= 0) {
                val radioModel1 = radioModelArrayList[index_pos]
               // Toast.makeText(context, " $pl", Toast.LENGTH_SHORT).show()
                val URL = radioModel1.link
                title.text = radioModel1.name
                playRadio(URL)
                Log.d("TAG", "link = " + URL!!)
            }
        }

        imgback.setOnClickListener {

            finish()
        }

    }

    fun stopService(){
        mp.stop()
        mp.reset()
    }
    fun MuteAudio() {
        val mAlramMAnager = this@RadioActivity.getSystemService(AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, AudioManager.ADJUST_MUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_ALARM, AudioManager.ADJUST_MUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_MUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_SYSTEM, AudioManager.ADJUST_MUTE, 0)
        } else {
            mAlramMAnager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_ALARM, true)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_MUSIC, true)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_RING, true)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_SYSTEM, true)
        }
    }
    fun UnMuteAudio() {
        val mAlramMAnager =this@RadioActivity.getSystemService(AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, AudioManager.ADJUST_UNMUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_ALARM, AudioManager.ADJUST_UNMUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_UNMUTE, 0)
            mAlramMAnager.adjustStreamVolume(AudioManager.STREAM_SYSTEM, AudioManager.ADJUST_UNMUTE, 0)
        } else {
            mAlramMAnager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_ALARM, false)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_MUSIC, false)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_RING, false)
            mAlramMAnager.setStreamMute(AudioManager.STREAM_SYSTEM, false)
        }
    }
    private fun getRadio() {
        radioModelArrayList = ArrayList()
        var radioModel = RadioModel()
        radioModel.name = "Alex jones"
        radioModel.link = "http://192.240.107.134/stream"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "DnB Radio"
        radioModel.link = "http://source.dnbradio.com:10128/;"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "Kera 90.1 FM"
        radioModel.link = "http://kera-ice.streamguys.us:80/keralive"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KAJA KJ97"
        radioModel.link = "https://c7icy.prod.playlists.ihrhls.com/2337_icy"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KAYD FM 101.7 KD 101"
        radioModel.link = "http://17843.live.streamtheworld.com:80/KAYDFMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBAT FM 99.9"
        radioModel.link = "http://54.235.62.118/townsquare-kbatfmaac-ibc3"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBCY FM 99.7"
        radioModel.link = "http://17843.live.streamtheworld.com:80/KBCYFMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBCY FM 99.7"
        radioModel.link = "http://17843.live.streamtheworld.com:80/KBCYFMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBED AM 1510"
        radioModel.link = "http://14023.live.streamtheworld.com:80/KIKRAMAAC_SC"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBEX-FM La Poderosa 96.1FM"
        radioModel.link = "http://net1.citrus3.com:9050/stream"
        radioModelArrayList.add(radioModel)
        radioModel = RadioModel()
        radioModel.name = "KBNR Mananatial FM 98.3"
        radioModel.link = "http://ic2.christiannetcast.com/kbnr-fm"
        radioModelArrayList.add(radioModel) //To change body of created functions use File | Settings | File Templates.
    }
    private fun playRadio(radio_url: String?) {
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC)
        stopme.isEnabled = true
        playme.isEnabled = false
        // get data from internet ...
        try {
            mp.setDataSource(radio_url)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // buffer...
        try {
            mp.prepare()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mp.start()
    }
}