package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dhaval Parmar on 2019-05-23.
 * Email : dhvlparmar7@gmail.com
 */
data class DirectionData(
    @SerializedName("status") var success: String = "",
    @SerializedName("message") var message: String = "",
    @SerializedName("geocoded_waypoints") var geocodedWaypoints: List<GeocodedWaypoints> = listOf(),
    @SerializedName("routes") var routes: List<Routes> = listOf()
)

data class GeocodedWaypoints(
    @SerializedName("geocoder_status") var geocoderStatus: String = "",
    @SerializedName("place_id") var placeId: String = ""
)

data class Routes(
    @SerializedName("legs") var legs: List<Legs> = listOf(),
    @SerializedName("overview_polyline") var overviewPolyLine: OverviewPolyLine = OverviewPolyLine()
)

data class Legs(@SerializedName("duration") var duration: TimeDuration = TimeDuration())

data class TimeDuration(
    @SerializedName("text") var time: String = "",
    @SerializedName("value") var seconds: Long = 0
)

data class OverviewPolyLine(
    @SerializedName("points") var points: String = ""
)