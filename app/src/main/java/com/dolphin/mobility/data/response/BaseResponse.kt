package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-08-05.
 * Email : dhvlparmar7@gmail.com
 */
abstract class BaseResponse(
    @SerializedName("message")
    var message: String = "",
    @SerializedName("statusCode")
    var statusCode: Int = 0
)

class CommonResponse() : BaseResponse()