package com.dolphin.mobility.data

/**
 * Created by Dhaval Parmar on 2019-05-18.
 * Email : dhvlparmar7@gmail.com
 */
data class HelpData(
    var title: String = "",
    var data: String = "",
    var isExpanded: Boolean = false,
    var type: Int = 0
)