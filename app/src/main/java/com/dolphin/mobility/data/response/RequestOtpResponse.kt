package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-06-11.
 * Email : dhvlparmar7@gmail.com
 */
data class RequestOtpResponse(
    @SerializedName("otp")
    var otp: Int = 0,
    @SerializedName("otpSent")
    var otpSent: Boolean = false,
    @SerializedName("userId")
    var userId: String = ""
)