package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dhaval Parmar on 2019-05-22.
 * Email : dhvlparmar7@gmail.com
 */
data class SearchLocation(
    @SerializedName("predictions") var predictions: List<Prediction> = listOf(),
    @SerializedName("status") var status: String = ""
)

data class Prediction(
    @SerializedName("description") var description: String = "",
    @SerializedName("id") var id: String = "",
    @SerializedName("matched_substrings") var matchedSubstrings: List<MatchedSubstring> = listOf(),
    @SerializedName("place_id") var placeId: String = "",
    @SerializedName("reference") var reference: String = "",
    @SerializedName("structured_formatting") var structuredFormatting: StructuredFormatting = StructuredFormatting(),
    @SerializedName("terms") var terms: List<Term> = listOf(),
    @SerializedName("types") var types: List<String> = listOf()
)

data class MatchedSubstring(
    @SerializedName("length") var length: Int = 0,
    @SerializedName("offset") var offset: Int = 0
)

data class Term(
    @SerializedName("offset") var offset: Int = 0,
    @SerializedName("value") var value: String = ""
)

data class StructuredFormatting(
    @SerializedName("main_text") var mainText: String = "",
    @SerializedName("main_text_matched_substrings") var mainTextMatchedSubstrings: List<MainTextMatchedSubstring> = listOf(),
    @SerializedName("secondary_text") var secondaryText: String = ""
)

data class MainTextMatchedSubstring(
    @SerializedName("length") var length: Int = 0,
    @SerializedName("offset") var offset: Int = 0
)