package com.dolphin.mobility.data.request

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-08-03.
 * Email : dhvlparmar7@gmail.com
 */
data class PostDeviceRequest(
    @SerializedName("deviceId")
    var deviceId: String = "",
    @SerializedName("vehicleNumber")
    var vehicleNumber: String = ""
)