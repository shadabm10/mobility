package com.dolphin.mobility.data.request

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-08-05.
 * Email : dhvlparmar7@gmail.com
 */
data class GetCouponRequest(
    @SerializedName("code")
    var code: String = "",
    @SerializedName("email")
    var email: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("phonenumber")
    var phonenumber: String = ""
)