package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-06-12.
 * Email : dhvlparmar7@gmail.com
 */
//data class VerifyOtpResponse(
//    @SerializedName("address")
//    var address: String = "",
//    @SerializedName("contactDetails")
//    var contactDetails: ContactDetails = ContactDetails(),
//    @SerializedName("email")
//    var email: String = "",
//    @SerializedName("_id")
//    var id: String = "",
//    @SerializedName("name")
//    var name: String = "",
//    @SerializedName("rideInfo")
//    var rideInfo: RideInfo = RideInfo(),
//    @SerializedName("__v")
//    var v: Int = 0
//)
//
//data class ContactDetails(
//    @SerializedName("primaryEmailId")
//    var primaryEmailId: String = "",
//    @SerializedName("primaryMobileNumber")
//    var primaryMobileNumber: String = ""
//)
//
//data class RideInfo(
//    @SerializedName("fleet")
//    var fleet: String = "",
//    @SerializedName("garageLocation")
//    var garageLocation: String = "",
//    @SerializedName("ride_per_week")
//    var ridePerWeek: String = ""
//)

data class VerifyOtpResponse(
    @SerializedName("address")
    var address: String = "",
    @SerializedName("city")
    var city: String = "",
    @SerializedName("contactDetails")
    var contactDetails: ContactDetails = ContactDetails(),
    @SerializedName("email")
    var email: String = "",
    @SerializedName("_id")
    var id: String = "",
    @SerializedName("imgUrl")
    var imgUrl: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("rideInfo")
    var rideInfo: RideInfo = RideInfo(),
    @SerializedName("summary")
    var summary: Any = Any(),
    @SerializedName("__v")
    var v: Int = 0
)

data class ContactDetails(
    @SerializedName("primaryEmailId")
    var primaryEmailId: String = "",
    @SerializedName("primaryMobileNumber")
    var primaryMobileNumber: String = ""
)

data class RideInfo(
    @SerializedName("fleet")
    var fleet: String = "",
    @SerializedName("garageLocation")
    var garageLocation: String = "",
    @SerializedName("licenseNumber")
    var licenseNumber: Any = Any(),
    @SerializedName("ride_per_week")
    var ridePerWeek: String = "",
    @SerializedName("vehicleNumber")
    var vehicleNumber: Any = Any()
)