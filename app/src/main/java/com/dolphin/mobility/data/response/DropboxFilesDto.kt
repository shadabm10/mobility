package com.dolphin.mobility.data.response

/**
 * Created by Dhaval Parmar on 2019-06-24.
 * Email : dhvlparmar7@gmail.com
 */
data class DropboxFilesDto(
    var fileUrl: String = "",
    var type: String = "image"
)