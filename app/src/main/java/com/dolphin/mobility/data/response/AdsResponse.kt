package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-05-04.
 * Email : dhvlparmar7@gmail.com
 */
data class AdsResponse(
    @SerializedName("filterCriteria")
    var filterCriteria: FilterCriteria = FilterCriteria(),
    @SerializedName("playList")
    var playList: List<Play> = listOf()
)

data class FilterCriteria(
    @SerializedName("age")
    var age: Int = 0,
    @SerializedName("gender")
    var gender: String = ""
)

data class Play(
    @SerializedName("activeTill")
    var activeTill: String = "",
    @SerializedName("_id")
    var id: String = "",
    @SerializedName("isActive")
    var isActive: Boolean = false,
    @SerializedName("mimeType")
    var mimeType: String = "",
    @SerializedName("uploadedBy")
    var uploadedBy: String = "",
    @SerializedName("url")
    var url: String = ""
)