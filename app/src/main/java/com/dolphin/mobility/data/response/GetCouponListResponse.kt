package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-08-05.
 * Email : dhvlparmar7@gmail.com
 */
data class GetCouponListResponse(
    @SerializedName("data")
    var data: List<CouponData> = listOf()
) : BaseResponse()

data class CouponData(
    @SerializedName("addedOn")
    var addedOn: String = "",
    @SerializedName("category")
    var category: String = "",
    @SerializedName("code")
    var code: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("discountPercentage")
    var discountPercentage: Int = 0,
    @SerializedName("_id")
    var id: String = "",
    @SerializedName("maxDiscountAmount")
    var maxDiscountAmount: Int = 0,
    @SerializedName("minPurchaseAmount")
    var minPurchaseAmount: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("validFrom")
    var validFrom: String = "",
    @SerializedName("validTill")
    var validTill: String = ""
)