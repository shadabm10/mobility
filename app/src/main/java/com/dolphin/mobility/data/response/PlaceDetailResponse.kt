package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dhaval Parmar on 2019-05-22.
 * Email : dhvlparmar7@gmail.com
 */
data class PlaceDetailResponse(
    @SerializedName("result")
    var result: PlaceDetailsResult = PlaceDetailsResult(),
    @SerializedName("status")
    var status: String = ""
)

data class PlaceDetailsResult(
    @SerializedName("geometry")
    var geometry: PlaceGeometry = PlaceGeometry(),
    @SerializedName("id")
    var id: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("place_id")
    var placeId: String = ""
)

data class PlaceGeometry(
    @SerializedName("location")
    var location: PlaceLocation = PlaceLocation()
)

data class PlaceLocation(
    @SerializedName("lat")
    var lat: Double = 0.0,
    @SerializedName("lng")
    var lng: Double = 0.0
)