package com.dolphin.mobility.data.request

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-06-11.
 * Email : dhvlparmar7@gmail.com
 */
data class RequestOtpRequest(
    @SerializedName("userId")
    var userId: String = ""
)