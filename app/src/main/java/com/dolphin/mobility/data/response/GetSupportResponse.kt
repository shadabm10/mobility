package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-08-05.
 * Email : dhvlparmar7@gmail.com
 */
data class GetSupportResponse(
    @SerializedName("data")
    var data: List<SupportData> = listOf()
) : BaseResponse()

data class SupportData(
    @SerializedName("helplineDescription")
    var helplineDescription: String = "",
    @SerializedName("helplineEmail")
    var helplineEmail: String = "",
    @SerializedName("helplineNumber")
    var helplineNumber: String = "",
    @SerializedName("_id")
    var id: String = "",
    @SerializedName("__v")
    var v: Int = 0
)