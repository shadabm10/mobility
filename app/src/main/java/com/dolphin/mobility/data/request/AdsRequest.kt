package com.dolphin.mobility.data.request

import com.dolphin.mobility.base.utils.Constants.MALE
import com.google.gson.annotations.SerializedName

/**
 * Created by Dhaval Parmar on 2019-05-27.
 * Email : dhvlparmar7@gmail.com
 */
data class AdsRequest(
    @SerializedName("age") var age: Int = 0,
    @SerializedName("gender") var gender: String = MALE
)