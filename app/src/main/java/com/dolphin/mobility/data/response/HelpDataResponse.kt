package com.dolphin.mobility.data.response

import com.google.gson.annotations.SerializedName


/**
 * Created by Dhaval Parmar on 2019-08-05.
 * Email : dhvlparmar7@gmail.com
 */
data class HelpDataResponse(
    @SerializedName("data")
    var data: List<HelpData> = listOf()
) : BaseResponse()

data class HelpData(
    @SerializedName("addedOn")
    var addedOn: String = "",
    @SerializedName("answer")
    var answer: String = "",
    @SerializedName("_id")
    var id: String = "",
    @SerializedName("question")
    var question: String = "",
    @SerializedName("__v")
    var v: Int = 0,
    var isExpanded: Boolean = false,
    var type: Int = 0
)