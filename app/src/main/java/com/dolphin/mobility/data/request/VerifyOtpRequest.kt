package com.dolphin.mobility.data.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * Created by Dhaval Parmar on 2019-06-12.
 * Email : dhvlparmar7@gmail.com
 */
@Parcelize
data class VerifyOtpRequest(
    @SerializedName("otp")
    var otp: String = "",
    @SerializedName("userId")
    var userId: String = ""
) : Parcelable