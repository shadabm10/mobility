package com.dolphin.mobility

import android.content.SharedPreferences
import android.location.Location
import android.preference.PreferenceManager
import androidx.multidex.MultiDexApplication
import com.facebook.ads.AudienceNetworkAds
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.FirebaseApp

/**
 * Created by Dhaval Parmar on 20/04/19.
 * Email : dhvlparmar7@gmail.com
 */

class DolphinMobilityApp : MultiDexApplication() {
    companion object {
        lateinit var instance: DolphinMobilityApp
        lateinit var sharedPreferences: SharedPreferences
        var currentLocation: Location? = null
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(instance)
        Fresco.initialize(instance)
        AudienceNetworkAds.initialize(instance)
        FirebaseApp.initializeApp(instance)
//        Stetho.initializeWithDefaults(this)
    }
}